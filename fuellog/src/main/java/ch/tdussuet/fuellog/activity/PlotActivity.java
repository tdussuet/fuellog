/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.room.Room;

import java.util.List;

import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.db.FuelDatabase;
import ch.tdussuet.fuellog.db.dao.GasRecordDao;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.db.entity.Vehicle;
import ch.tdussuet.fuellog.plot.CostPlot;
import ch.tdussuet.fuellog.plot.GallonsPlot;
import ch.tdussuet.fuellog.plot.MileagePlot;
import ch.tdussuet.fuellog.plot.OdometerPlot;
import ch.tdussuet.fuellog.plot.PricePlot;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.ui.PlotDateRangeButtons;
import ch.tdussuet.fuellog.util.DateComparator;
import ch.tdussuet.fuellog.util.GasRecordUtil;
import ch.tdussuet.fuellog.util.OldMonthlyTrips;
import ch.tdussuet.fuellog.util.PlotFontSize;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.Getter;
import timber.log.Timber;

/**
 * DESCRIPTION:
 * Implements a group of tabs containing plots for economy,
 * gasoline purchased, and distance driven statistics.
 */
public class PlotActivity extends FragmentActivity implements OnSharedPreferenceChangeListener {

    /// a tag string for debug logging (the name of this class)
    @SuppressWarnings("unused")
    private static final String TAG = PlotActivity.class.getName();

    /// key name for the Vehicle to pass via Intent
    /// gas records for this vehicle are the data that is plotted
    public static final String VEHICLE = PlotActivity.class.getName() + ".VEHICLE";

    /// the data to plot
    @Getter
    private List<GasRecord> records = null;

    /// the plots
    private final MileagePlot plotMileage = new MileagePlot();
    private final OdometerPlot plotOdometer = new OdometerPlot();
    private final GallonsPlot plotGallons = new GallonsPlot();
    private final CostPlot plotCost = new CostPlot();
    private final PricePlot plotPrice = new PricePlot();

    /// buttons for selection of range of data to evaluate
    @SuppressWarnings("unused")
    private PlotDateRangeButtons rangeButtons;

    private ScrollView scrollview;

    private final CompositeDisposable allDisposables = new CompositeDisposable();

    public PlotActivity() {
        super(R.layout.activity_plot);
    }

    /**
     * DESCRIPTION:
     * Called when the Activity is created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the vehicle from Intent
        Intent intent = getIntent();
        Vehicle vehicle = (Vehicle) intent.getSerializableExtra(VEHICLE);

        FuelDatabase database = Room.databaseBuilder(getApplicationContext(), FuelDatabase.class, FuelDatabase.DB_NAME).build();
        GasRecordDao gasRecordDao = database.gasRecordDao();
        var disposable = gasRecordDao.findAllByVehicle(vehicle.getId())
                .subscribeOn(Schedulers.io())
                .subscribe(recordsByVechile -> {
                            records = recordsByVechile;
                            records.sort(new DateComparator());
                            setupView();
                        },
                        Timber::e);
        allDisposables.add(disposable);
    }

    private void setupView() {
        // calculate monthly totals
        OldMonthlyTrips monthly = new OldMonthlyTrips(records, this);

        // initialize the plot range buttons
        rangeButtons = new PlotDateRangeButtons(this, SettingsActivity.KEY_PLOT_DATE_RANGE);

        // create plots
        plotMileage.createPlot(this, findViewById(R.id.xyMileagePlot), monthly);
        plotOdometer.createPlot(this, findViewById(R.id.xyOdometerPlot), monthly);
        plotGallons.createPlot(this, findViewById(R.id.xyGallonsPlot), monthly);
        plotCost.createPlot(this, findViewById(R.id.xyCostPlot), monthly);
        plotPrice.createPlot(this, findViewById(R.id.xyPricePlot), monthly);

        // set font size for plot titles to reflect preferences
        setTitlesFontSize();

        // setup to adjust plot height to fit on screen once layout size is known
        scrollview = findViewById(R.id.scrollviewPlots);
        ViewTreeObserver vto = scrollview.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int height = (scrollview.getHeight() * 85) / 100;
                plotMileage.setHeight(height);
                plotOdometer.setHeight(height);
                plotGallons.setHeight(height);
                plotCost.setHeight(height);
                plotPrice.setHeight(height);
                // remove this listener or it will repeatedly run
                ViewTreeObserver vto = scrollview.getViewTreeObserver();
                if (vto.isAlive()) {
                    vto.removeOnGlobalLayoutListener(this);
                }
            }
        });

        // setup to be notified when shared preferences change
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        allDisposables.clear();
    }

    /**
     * DESCRIPTION:
     * Initialize the contents of the Activity's standard options menu.
     *
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_plot, menu);
        return true;
    }

    /**
     * DESCRIPTION:
     * Called when an item in the options menu is selected.
     *
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.itemSettings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onContextItemSelected(item);
    }

    /**
     * DESCRIPTION:
     * Called when one or more shared preferences have changed.
     *
     * @see android.content.SharedPreferences.OnSharedPreferenceChangeListener#onSharedPreferenceChanged(android.content.SharedPreferences, java.lang.String)
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        // update the data to reflect new units
        if (key.equals(SettingsActivity.KEY_UNITS)) {
            GasRecordUtil.calculateMileage(records, this);
            OldMonthlyTrips monthly = new OldMonthlyTrips(records, this);
            plotOdometer.setMonthly(monthly);
            plotGallons.setMonthly(monthly);
            plotCost.setMonthly(monthly);
            plotPrice.setMonthly(monthly);
        }
        if (key.equals(SettingsActivity.KEY_PLOT_FONT_SIZE)) {
            setTitlesFontSize();
        }
        // notify plots that preferences have changed
        plotMileage.onSharedPreferenceChanged(sharedPreferences, key);
        plotOdometer.onSharedPreferenceChanged(sharedPreferences, key);
        plotGallons.onSharedPreferenceChanged(sharedPreferences, key);
        plotCost.onSharedPreferenceChanged(sharedPreferences, key);
        plotPrice.onSharedPreferenceChanged(sharedPreferences, key);
    }

    /**
     * DESCRIPTION:
     * Adjust font size used for plot title labels to reflect shared
     * preferences.
     */
    private void setTitlesFontSize() {
        PlotFontSize size = new PlotFontSize(this, SettingsActivity.KEY_PLOT_FONT_SIZE);
        float sizeTitle = size.getSizeDp() + 2.0f;
        TextView title;
        title = findViewById(R.id.titleMileagePlot);
        title.setTextSize(sizeTitle);
        title = findViewById(R.id.titleOdometerPlot);
        title.setTextSize(sizeTitle);
        title = findViewById(R.id.titleGallonsPlot);
        title.setTextSize(sizeTitle);
        title = findViewById(R.id.titleCostPlot);
        title.setTextSize(sizeTitle);
        title = findViewById(R.id.titlePricePlot);
        title.setTextSize(sizeTitle);
    }
}
