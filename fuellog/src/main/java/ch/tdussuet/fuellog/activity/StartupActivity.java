/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import ch.tdussuet.fuellog.App;
import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.db.FuelDatabase;
import ch.tdussuet.fuellog.dialog.UnitsDialogFragment;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.util.Utilities;
import ch.tdussuet.fuellog.util.format.CurrencyManager;
import timber.log.Timber;

/**
 * DESCRIPTION:
 * An Activity that performs startup checks and initialization for
 * the application, then launches the main Activity.
 */
public class StartupActivity extends FragmentActivity implements UnitsDialogFragment.Listener {

    protected static final String TAG = StartupActivity.class.getSimpleName();

    public StartupActivity() {
        super(R.layout.activity_startup);
    }

    /**
     * DESCRIPTION
     * Called when the activity is starting.
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // currency manager initialization can be time consuming...get it out of the way at startup
        CurrencyManager.getInstance(this);

        // special case: display update information
        if (isUpdateFirstStart()) {
            showUpdateInformation();
            return;
        }

        // special case: prompt user to select units of measurement
        if (isInstallFirstStart()) {
            DialogFragment dialogFragment = UnitsDialogFragment.create(this, this);
            dialogFragment.show(getSupportFragmentManager(), "unitsDialog");
            return;
        }

        // normal startup - start the application's main activity
        startMainActivity();
    }

    /**
     * DESCRIPTION:
     * Starts the main Activity and exits this startup activity.
     */
    //TODO: add bundle?
    protected void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * DESCRIPTION:
     * Determines if this is the first time starting after a
     * clean application install (no data exists yet).
     *
     * @return true if this is first startup after a clean install.
     */
    protected boolean isInstallFirstStart() {
        return FuelDatabase.isDatabaseFileMissing(this);
    }

    /**
     * DESCRIPTION:
     * Determines if this is the first time starting after an application
     * update.
     *
     * @return true if first startup after an application update.
     */
    protected boolean isUpdateFirstStart() {

        final String KEY = StartupActivity.class.getName() + ".savedVersionCode";
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);

        // get the saved version code
        int savedVersionCode = prefs.getInt(KEY, 0);

        // get the current version code for this application
        int appVersionCode = App.getVersionCode(this);

        // save the current version code so we only do this once per version
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(KEY, appVersionCode);
        editor.apply();

        // if the database does not exist yet, then this is not an update
        if (FuelDatabase.isDatabaseFileMissing(this)) {
            return false;
        }

        // decide whether to display update info based on the saved version
        return savedVersionCode < 7;

        // yes, this is first startup for this version of the app
    }

    /**
     * DESCRIPTION:
     * Displays information to the user describing what has changed
     * for the current software update, if the update.html exists.
     */
    protected void showUpdateInformation() {

        // if update.html does not exist, skip display and start main activity
        if (!App.existsUpdateHtml(this)) {
            String tag = TAG + ".showUpdateInfo()";
            String msg = "UPDATE HTML FILE DOES NOT EXIST";
            Timber.d(tag, msg);
            startMainActivity();
            return;
        }

        // start an activity to display the update information
        // and await a result from the activity
        Intent intent = new Intent(this, HtmlViewerActivity.class);
        intent.putExtra(HtmlViewerActivity.URL, getString(R.string.url_update_html));
        intent.putExtra(HtmlViewerActivity.RETURN_RESULT, true);
        startActivityForResult(intent, SHOW_UPDATE_INFO);
    }

    /**
     * DESCRIPTION:
     * Request code constants for onActivityResult()
     *
     * @see #onActivityResult(int, int, Intent)
     */
    protected static final int SHOW_UPDATE_INFO = 1;

    /**
     * DESCRIPTION:
     * Called when an activity launched by this activity exits, giving the
     * requestCode it was started with, the resultCode it returned, and any
     * additional data from it.
     *
     * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode != SHOW_UPDATE_INFO) {
            Utilities.toast(this, "Invalid request code.");
        }
        startMainActivity();
    }

    /**
     * DESCRIPTION:
     * Exits this Activity with launching the main Activity.
     */
    protected void exitApplication() {
        finish();
    }

    /**
     * DECSRIPTION:
     * Called when a user response has been obtained from the dialog.
     *
     * @see UnitsDialogFragment.Listener#onUnitsDialogResponse(UnitsDialogFragment.Result, String)
     */
    @Override
    public void onUnitsDialogResponse(UnitsDialogFragment.Result result, String value) {

        switch (result) {
            case RESULT_SELECTED:

                // save the selected units of measurement as a shared preference
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(SettingsActivity.KEY_UNITS, value);
                editor.apply();

                // done
                startMainActivity();
                break;

            case RESULT_CANCEL:
                exitApplication();
                break;
        }
    }
}
