/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.room.Room;

import java.util.LinkedList;
import java.util.List;

import ch.tdussuet.fuellog.BuildConfig;
import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.db.FuelDatabase;
import ch.tdussuet.fuellog.db.dao.GasRecordDao;
import ch.tdussuet.fuellog.db.dao.VehicleDao;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.db.entity.Vehicle;
import ch.tdussuet.fuellog.dialog.ConfirmationDialogFragment;
import ch.tdussuet.fuellog.dialog.MileageCalculationDialogFragment;
import ch.tdussuet.fuellog.dialog.MileageEstimateDialogFragment;
import ch.tdussuet.fuellog.dialog.TankNeverFilledDialogFragment;
import ch.tdussuet.fuellog.dialog.VehicleDialogFragment;
import ch.tdussuet.fuellog.item.GasRecordExporter;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.util.Utilities;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.Getter;
import lombok.Setter;
import timber.log.Timber;

/**
 * DESCRIPTION:
 * The main Activity for the application. Displays a drop down list of
 * vehicles from the log and allows a user to add/edit/or delete vehicles,
 * view vehicle logs, and add a gas record to the log for a selected a
 * vehicle.
 */
public class MainActivity extends FragmentActivity implements VehicleDialogFragment.Listener, ConfirmationDialogFragment.Listener, View.OnClickListener {

    /// the gas log
    @Getter
    @Setter
    private GasRecordExporter gaslog;

    /// a list of vehicles from the log
    private List<Vehicle> vehicles;

    /// the selected vehicle
    private Vehicle selectedVehicle;

    /// a "drop down list" of vehicles
    @Getter
    @Setter
    private Spinner spinnerVehicles;

    /// list of views that require an existing vehicle
    @Getter
    @Setter
    private List<View> listViewsThatNeedVehicle;

    /// an adapter to populate the spinner with vehicle names
    @Getter
    @Setter
    private ArrayAdapter<Vehicle> adapter;

    VehicleDao vehicleDao;
    GasRecordDao gasRecordDao;

    private final CompositeDisposable disposable = new CompositeDisposable();

    public MainActivity() {
        super(R.layout.activity_main);
    }

    /**
     * DESCRIPTION
     * Called when the activity is starting.
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        FuelDatabase database = Room.databaseBuilder(getApplicationContext(), FuelDatabase.class, FuelDatabase.DB_NAME).build();
        vehicleDao = database.vehicleDao();
        gasRecordDao = database.gasRecordDao();

        var allVehicles = vehicleDao.getAll();
        disposable.add(allVehicles.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(daoVehicles -> vehicles = daoVehicles,
                        throwable -> Timber.e(throwable, "Unable to get vehicles")));

        // restore preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        // register as click listener for our views
        ImageButton buttonVehicleAdd = findViewById(R.id.buttonVehicleAdd);
        ImageButton buttonVehicleEdit = findViewById(R.id.buttonVehicleEdit);
        ImageButton buttonVehicleDelete = findViewById(R.id.buttonVehicleDelete);
        Button buttonGetGas = findViewById(R.id.buttonGetGas);
        Button buttonViewLog = findViewById(R.id.buttonViewLog);
        Button buttonPlotData = findViewById(R.id.buttonPlotData);
        Button buttonViewStatistics = findViewById(R.id.buttonViewStatistics);
        buttonVehicleAdd.setOnClickListener(this);
        buttonVehicleEdit.setOnClickListener(this);
        buttonVehicleDelete.setOnClickListener(this);
        buttonGetGas.setOnClickListener(this);
        buttonViewLog.setOnClickListener(this);
        buttonPlotData.setOnClickListener(this);
        buttonViewStatistics.setOnClickListener(this);

        // create a list of views that require an existing vehicle
        listViewsThatNeedVehicle = new LinkedList<>();
        listViewsThatNeedVehicle.add(buttonVehicleEdit);
        listViewsThatNeedVehicle.add(buttonVehicleDelete);
        listViewsThatNeedVehicle.add(buttonGetGas);
        listViewsThatNeedVehicle.add(buttonViewLog);
        listViewsThatNeedVehicle.add(buttonPlotData);
        listViewsThatNeedVehicle.add(buttonViewStatistics);

        // create a log instance for use by this application
        gaslog = new GasRecordExporter(this);

        // create a drop down list for vehicle selection
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, vehicles);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerVehicles = findViewById(R.id.spinnerVehicles);
        spinnerVehicles.setAdapter(adapter);
        updateVehiclesSpinnerState();

        // if the activity is not being re-initialized (for example after screen rotate)
        // start by adding a vehicle if there are none currently defined
        if ((savedInstanceState == null) && vehicles.isEmpty()) {
            showDialogFragment(DIALOG_ADD_VEHICLE_ID);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        disposable.clear();
    }

    /**
     * DESCRIPTION:
     * Enable/disable the "drop down list" of vehicles and all
     * vehicle related View instances depending on whether any vehicles
     * are currently defined.
     */
    private void updateVehiclesSpinnerState() {
        boolean enabled = !vehicles.isEmpty();
        spinnerVehicles.setEnabled(enabled);
        for (View view : listViewsThatNeedVehicle) {
            view.setEnabled(enabled);
        }
    }

    /**
     * DESCRIPTION:
     * Initialize the contents of the Activity's standard options menu.
     *
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    /**
     * DESCRIPTION:
     * Called when an item in the options menu is selected.
     *
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent;

        int itemId = item.getItemId();
        if (itemId == R.id.itemVehicleAdd) {
            showDialogFragment(DIALOG_ADD_VEHICLE_ID);
            return true;
        } else if (itemId == R.id.itemVehicleEdit) {
            if (getSelectedVehicle() != null) {
                showDialogFragment(DIALOG_EDIT_VEHICLE_ID);
            }
            return true;
        } else if (itemId == R.id.itemVehicleDelete) {
            if (getSelectedVehicle() != null) {
                showDialogFragment(DIALOG_DELETE_VEHICLE_ID);
            }
            return true;
        } else if (itemId == R.id.itemHelp) {
            intent = new Intent(this, HtmlViewerActivity.class);
            intent.putExtra(HtmlViewerActivity.URL, getString(R.string.url_help_html));
            startActivity(intent);
            return true;
        } else if (itemId == R.id.itemSettings) {
            intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * DESCRIPTION:
     * Called when a View (i.e Button) that this Activity is a registered
     * listener for is clicked.
     *
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {

        int id = v.getId();
        if (id == R.id.buttonVehicleAdd) {
            showDialogFragment(DIALOG_ADD_VEHICLE_ID);
        } else if (id == R.id.buttonVehicleEdit) {
            if (getSelectedVehicle() != null) {
                showDialogFragment(DIALOG_EDIT_VEHICLE_ID);
            }
        } else if (id == R.id.buttonVehicleDelete) {
            if (getSelectedVehicle() != null) {
                showDialogFragment(DIALOG_DELETE_VEHICLE_ID);
            }
        } else if (id == R.id.buttonGetGas) {
            getGas(v);
        } else if (id == R.id.buttonViewLog) {
            viewLog(v);
        } else if (id == R.id.buttonPlotData) {
            plotData(v);
        } else if (id == R.id.buttonViewStatistics) {
            viewStatistics(v);
        } else {
            Utilities.toast(this, "Invalid view id.");
        }

    }

    /**
     * DESCRIPTION:
     * Determines which vehicle is currently selected in the spinner.
     *
     * @return the selected Vehicle. Also sets the selectedVehicle class attribute.
     */
    protected Vehicle getSelectedVehicle() {
        selectedVehicle = (Vehicle) spinnerVehicles.getSelectedItem();
        if (selectedVehicle == null) {
            Utilities.toast(this, getString(R.string.toast_select_a_vehicle));
        }
        return selectedVehicle;
    }

    /**
     * DESCRIPTION:
     * Selects a vehicle based on its position in the vehicles list.
     *
     * @param position - the list index of the vehicle to select
     */
    protected void setSelectedVehicle(int position) {
        updateVehiclesSpinnerState();
        if ((position >= 0) && (position < vehicles.size())) {
            spinnerVehicles.setSelection(position);
        }
    }

    /**
     * DESCRIPTION:
     * Locates a specified vehicle in the spinner and selects it. If unable
     * to find vehicle, the first vehicle in spinner is selected.
     *
     * @param name - the name of the vehicle to select.
     */
    protected void setSelectedVehicle(String name) {

        // find the vehicle by name
        int position = 0;
        for (int i = 0; i < vehicles.size(); i++) {
            if (vehicles.get(i).getName().equals(name)) {
                position = i;
            }
        }

        // select it
        setSelectedVehicle(position);
    }

    /**
     * DESCRIPTION:
     * Starts an Activity to view the log records for the
     * selected vehicle.
     *
     * @param view - currently unused - necessary only if called as click listener.
     */
    public void viewLog(View view) {

        // get the selected vehicle
        if (getSelectedVehicle() == null) return;

        // start an Activity to display gas records for the vehicle
        Intent intent = new Intent(this, GasLogListActivity.class);
        intent.putExtra(GasLogListActivity.VEHICLE, selectedVehicle);
        startActivity(intent);
    }

    /**
     * DESCRIPTION:
     * Starts an Activity to get a new gasoline record to be added to
     * the gas log for the selected vehicle.
     *
     * @param view - currently unused - necessary only if called as click listener.
     */
    public void getGas(View view) {

        // get the selected vehicle
        if (getSelectedVehicle() == null) return;

        // prepare input for GasRecordActivity
        GasRecord gasRecord = new GasRecord();
        gasRecord.setVehicleID(selectedVehicle.getId());
        int currentOdometer = gasRecordDao.getCurrentOdometer(selectedVehicle.getId()).blockingGet();

        // start a GasRecordActivity to get a new gas record for the vehicle
        Intent intent = new Intent(this, GasRecordActivity.class);
        intent.putExtra(GasRecordActivity.RECORD, gasRecord);
        intent.putExtra(GasRecordActivity.CURRENT_ODOMETER, currentOdometer);
        intent.putExtra(GasRecordActivity.TANK_SIZE, selectedVehicle.getTankSize());
        startActivityForResult(intent, GET_GAS_REQUEST);
    }

    /**
     * DESCRIPTION:
     * Starts an Activity to plot data for the selected vehicle.
     *
     * @param view - currently unused - necessary only if called as click listener.
     */
    public void plotData(View view) {

        // get the selected vehicle
        if (getSelectedVehicle() == null) return;

        // start an Activity to display gas records for the vehicle
        Intent intent = new Intent(this, PlotActivity.class);
        intent.putExtra(PlotActivity.VEHICLE, selectedVehicle);
        startActivity(intent);
    }

    /**
     * DESCRIPTION:
     * Starts an Activity to display statistical data for the selected vehicle.
     *
     * @param view - currently unused - necessary only if called as click listener.
     */
    public void viewStatistics(View view) {

        // get the selected vehicle
        if (getSelectedVehicle() == null) return;

        // start an Activity to display statistics for the vehicle
        Intent intent = new Intent(this, StatisticsActivity.class);
        intent.putExtra(StatisticsActivity.VEHICLE_INTENT_KEY, selectedVehicle);
        startActivity(intent);
    }

    /**
     * DESCRIPTION:
     * Adds a new vehicle to the log.
     *
     * @param vehicle - the new Vehicle.
     */
    protected void addVehicle(Vehicle vehicle) {

        vehicleDao.insert(vehicle).doOnError(e ->
            Utilities.toast(this, getString(R.string.toast_add_failed))
        );

        reloadVehiclesAndSelect(vehicle);
    }

    private void reloadVehiclesAndSelect(Vehicle vehicle) {
        // read updated list of vehicles from the log
        var allVehicles = vehicleDao.getAll();
        disposable.add(allVehicles.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(daoVehicles -> {
                            vehicles.clear();
                            vehicles.addAll(daoVehicles);
                            adapter.notifyDataSetChanged();

                            // select the new vehicle by its name
                            if (vehicle != null) {
                                setSelectedVehicle(vehicle.getName());
                            } else {
                                setSelectedVehicle(0);
                            }
                        },
                        throwable -> Timber.e(throwable, "Unable to get vehicles")));
    }

    /**
     * DESCRIPTION:
     * Updates data for a specified vehicle in the log.
     *
     * @param vehicle - the edited vehicle.
     */
    protected void editVehicle(Vehicle vehicle) {

        vehicleDao.update(vehicle).doOnError(e ->
            Utilities.toast(this, getString(R.string.toast_edit_failed))
        );

        reloadVehiclesAndSelect(vehicle);
    }

    /**
     * DESCRIPTION:
     * Removes the selected vehicle and all its gas records from
     * the log.
     */
    protected void deleteVehicle() {

        vehicleDao.delete(selectedVehicle).doOnError(e ->
            Utilities.toast(this, getString(R.string.toast_delete_failed))
        );

        reloadVehiclesAndSelect(null);
    }

    /**
     * DESCRIPTION:
     * Dialog box integer ID constants
     *
     * @see #onCreateDialog(int)
     */
    protected static final int DIALOG_ADD_VEHICLE_ID = 1;
    protected static final int DIALOG_EDIT_VEHICLE_ID = 2;
    protected static final int DIALOG_DELETE_VEHICLE_ID = 3;

    /**
     * DESCRIPTION:
     * Called as needed by the framework to create dialog boxes used by the Activity.
     * Each dialog box is referenced by a locally defined id integer.
     *
     * @see android.app.Activity#onCreateDialog(int)
     */
    private void showDialogFragment(int id) {
        DialogFragment dialog;
        String title;
        String message;

        switch (id) {
            case DIALOG_ADD_VEHICLE_ID:
                title = getString(R.string.vehicle_add_label);
                dialog = VehicleDialogFragment.create(this, this, id, title, new Vehicle(0, ""));
                dialog.show(getSupportFragmentManager(), "vehicleDialog");
                break;

            case DIALOG_EDIT_VEHICLE_ID:
                title = getString(R.string.vehicle_edit_label);
                dialog = VehicleDialogFragment.create(this, this, id, title, selectedVehicle);
                dialog.show(getSupportFragmentManager(), "vehicleDialog");
                break;

            case DIALOG_DELETE_VEHICLE_ID:
                title = getString(R.string.title_confirm_delete_vehicle);
                message = getString(R.string.message_confirm_delete_vehicle);
                dialog = ConfirmationDialogFragment.create(this, this, id, title, message);
                dialog.show(getSupportFragmentManager(), "confirmDialog");
                break;

            default:
                Utilities.toast(this, "Invalid dialog id.");
        }
    }

    /**
     * DESCRIPTION:
     * Called when the user is done entering/editing vehicle data.
     *
     * @see VehicleDialogFragment.Listener#onVehicleDialogClosure(int, Vehicle)
     */
    @Override
    public void onVehicleDialogClosure(int id, Vehicle vehicle) {

        dismissDialog("vehicleDialog");
        if (vehicle == null) {
            Utilities.toast(this, getString(R.string.toast_canceled));
            return;
        }

        switch (id) {

            case DIALOG_ADD_VEHICLE_ID:
                addVehicle(vehicle);
                break;

            case DIALOG_EDIT_VEHICLE_ID:
                editVehicle(vehicle);
                break;

            default:
                Utilities.toast(this, "Invalid dialog id.");
        }
    }

    /**
     * DECSRIPTION:
     * Called when a user response has been obtained from the dialog.
     *
     * @see ConfirmationDialogFragment.Listener#onConfirmationDialogResponse(int, boolean)
     */
    @Override
    public void onConfirmationDialogResponse(int id, boolean confirmed) {

        dismissDialog("confirmDialog");

        if (!confirmed) return;

        if (id == DIALOG_DELETE_VEHICLE_ID) {
            deleteVehicle();
        } else {
            Utilities.toast(this, "Invalid dialog id: " + id);
        }
    }

    /**
     * DESCRIPTION:
     * Adds a gas record to the log for the selected vehicle.
     *
     * @param gasRecord - the GasRecord data entered by the user.
     */
    protected void addGasRecord(GasRecord gasRecord) {

        // get a list of records from the log before adding new record
        List<GasRecord> list = gasRecordDao.findAllByVehicle(selectedVehicle.getId()).blockingGet();

        gasRecord.setVehicleID(selectedVehicle.getId());
        gasRecordDao.insert(gasRecord).doOnError(e -> {
            Utilities.toast(this, getString(R.string.toast_error_saving_data));
            return;
        });

        // success!
        Utilities.toast(this, getString(R.string.toast_data_saved));

        // need a previous full tank in the log to do any calculations
        if (TankNeverFilledDialogFragment.isDisplayable(list)) {
            TankNeverFilledDialogFragment dialogFragment = TankNeverFilledDialogFragment.create(this);
            dialogFragment.show(getSupportFragmentManager(), "tankNeverFilledDialog");
            return;
        }

        // add the new record to the list
        list.add(gasRecord);

        // recalculate mileage
//        TODO fix
//        GasRecordList.calculateMileage(list, this);

        // find the new location of the record in the list (after sort)
//        int location = GasRecordList.find(list, gasRecord);
        int location = list.size();
        // display mileage calculation if possible
        if (MileageCalculationDialogFragment.isDisplayable(gasRecord)) {
            DialogFragment dialogFragment = MileageCalculationDialogFragment.create(this, gasRecord);
            dialogFragment.show(getSupportFragmentManager(), "mileageCalcDialog");
            return;
        }

        // display mileage estimate if possible
        if (MileageEstimateDialogFragment.isDisplayable(selectedVehicle, list, location)) {
            DialogFragment dialogFragment = MileageEstimateDialogFragment.create(this, selectedVehicle, list, location);
            dialogFragment.show(getSupportFragmentManager(), "mileageEstimateDialog");
        }

    }

    /**
     * DESCRIPTION:
     * Request code constants for onActivityResult()
     *
     * @see #onActivityResult(int, int, Intent)
     */
    private static final int GET_GAS_REQUEST = 1;

    /**
     * DESCRIPTION:
     * Called when an activity launched by this activity exits, giving the
     * requestCode it was started with, the resultCode it returned, and any
     * additional data from it.
     *
     * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == GET_GAS_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                GasRecord gasRecord = (GasRecord) intent.getSerializableExtra(GasRecordActivity.RECORD);
                addGasRecord(gasRecord);
            } else {
                Utilities.toast(this, getString(R.string.toast_canceled));
            }
        } else {
            Utilities.toast(this, "Invalid Request Code.");
        }
    }

    /**
     * DESCRIPTION:
     * Save current state data.
     * NOTE: This gets called when the screen is rotated. The
     * Activity is then destroyed, re-created, and state restored.
     *
     * @see android.app.Activity#onSaveInstanceState(android.os.Bundle)
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("selectedVehicle", selectedVehicle);
    }

    /**
     * DESCRIPTION:
     * Restore previously saved state data.
     *
     * @see android.app.Activity#onRestoreInstanceState(android.os.Bundle)
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        selectedVehicle = (Vehicle) savedInstanceState.getSerializable("selectedVehicle");
    }

    private void dismissDialog(String tag) {
        Fragment prev = getSupportFragmentManager().findFragmentByTag(tag);
        if (prev != null) {
            DialogFragment df = (DialogFragment) prev;
            df.dismiss();
        }
    }

}
