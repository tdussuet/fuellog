/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.PreferenceManager;
import androidx.room.Room;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.adapter.GasLogListAdapter;
import ch.tdussuet.fuellog.db.FuelDatabase;
import ch.tdussuet.fuellog.db.dao.GasRecordDao;
import ch.tdussuet.fuellog.db.dao.VehicleDao;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.db.entity.Vehicle;
import ch.tdussuet.fuellog.dialog.ConfirmationDialogFragment;
import ch.tdussuet.fuellog.dialog.MileageCalculationDialogFragment;
import ch.tdussuet.fuellog.dialog.MileageEstimateDialogFragment;
import ch.tdussuet.fuellog.dialog.StorageSelectionDialogFragment;
import ch.tdussuet.fuellog.dialog.TankNeverFilledDialogFragment;
import ch.tdussuet.fuellog.item.ExternalStorage;
import ch.tdussuet.fuellog.item.GasRecordExporter;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.preferences.Units;
import ch.tdussuet.fuellog.util.Utilities;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import timber.log.Timber;

/**
 * DESCRIPTION:
 * Implements an Android Activity class to display a list of
 * gasoline records contained within a gas log database. Provides
 * a user interface to manipulate records in the log.
 */
public class GasLogListActivity extends FragmentActivity implements ConfirmationDialogFragment.Listener,
        StorageSelectionDialogFragment.Listener,
        OnItemClickListener,
        OnSharedPreferenceChangeListener {

    /// key name for the Vehicle to pass via Intent
    /// gas records for this vehicle are displayed in the list
    public static final String VEHICLE = GasLogListActivity.class.getName() + ".VEHICLE";

    /// the gasoline log
    private GasRecordExporter gaslog;

    /// the vehicle to display gas records for (obtained via Intent)
    private Vehicle vehicle;

    /// a list of records in the log
    private List<GasRecord> records;

    /// the currently selected row from the list of records
    private int selectedRow;

    /// the Android ListView for display of log records
    private ListView listView;

    /// an adapter used to format and display each log record
    private GasLogListAdapter adapter;

    VehicleDao vehicleDao;
    GasRecordDao gasRecordDao;

    private final CompositeDisposable allDisposables = new CompositeDisposable();

    public GasLogListActivity() {
        super(R.layout.activity_gas_log_list);
    }

    /**
     * DESCRIPTION:
     * Called when the activity is starting.
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FuelDatabase database = Room.databaseBuilder(getApplicationContext(), FuelDatabase.class, FuelDatabase.DB_NAME).build();
        vehicleDao = database.vehicleDao();
        gasRecordDao = database.gasRecordDao();

        // get the vehicle from Intent
        Intent intent = getIntent();
        vehicle = (Vehicle) intent.getSerializableExtra(VEHICLE);

        // initialize other attributes
        gaslog = new GasRecordExporter(this);
        var disposable = gasRecordDao.getAll()
                .subscribeOn(Schedulers.io())
                .subscribe(allGasRecords -> records = allGasRecords,
                        Timber::e);
        allDisposables.add(disposable);
        listView = findViewById(R.id.gas_log_list);
        adapter = new GasLogListAdapter(this, records);

        // configure ListView to use our adapter
        listView.setAdapter(adapter);

        // configure ListView to use our context menu when a record is clicked
        registerForContextMenu(listView);
        listView.setLongClickable(false);
        listView.setOnItemClickListener(this);

        // scroll ListView to last record (highest odometer value)
        if (listView.getCount() > 0) {
            listView.setSelection(listView.getCount() - 1);
        }

        // set column header labels to reflect current unit preference
        updateColumnHeaderLabels();

        // setup to be notified when preferences change
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        allDisposables.clear();
    }

    /**
     * DESCRIPTION:
     * Initialize the Activity's standard options menu. This is only called
     * once, the first time the options menu is displayed.
     *
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_gas_log_list, menu);
        return true;
    }

    /**
     * DESCRIPTION:
     * Prepare the Activity's standard options menu to be displayed. This
     * is called right before the menu is shown, every time it is shown,
     * and can therefore be used to efficiently enable/disable items or
     * otherwise dynamically modify the contents.
     *
     * @see android.app.Activity#onPrepareOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem itemExport = menu.findItem(R.id.itemExport);
        itemExport.setEnabled(!records.isEmpty());
        return true;
    }

    /**
     * DESCRIPTION:
     * Called when an item in the options menu is selected.
     *
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == R.id.itemGetGas) {
            getGas();
            return true;
        } else if (itemId == R.id.itemImport) {
            DialogFragment dialogFragment = StorageSelectionDialogFragment.create(this, this);
            dialogFragment.show(getSupportFragmentManager(), "storageDialog");
            return true;
        } else if (itemId == R.id.itemExport) {
            if (records.isEmpty()) {
                Utilities.toast(this, getString(R.string.toast_no_data_to_export));
            } else {
                exportData();
            }
            return true;
        } else if (itemId == R.id.itemSettings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * DESCRIPTION:
     * Called every time the context menu is about to be shown.
     *
     * @see android.app.Activity#onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);

        // create the menu
        getMenuInflater().inflate(R.menu.context_gas_log_list, menu);

        // get index of currently selected row
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
        selectedRow = (int) info.id;

        // get record that is currently selected
        GasRecord gasRecord = records.get(selectedRow);

        // adjust menu contents for "show estimate"
        if (!MileageEstimateDialogFragment.isDisplayable(vehicle, records, selectedRow)) {
            menu.removeItem(R.id.itemShowEstimate);
        }

        // adjust menu contents for "show calculation"
        if (!MileageCalculationDialogFragment.isDisplayable(gasRecord)) {
            menu.removeItem(R.id.itemShowCalc);
        }

        // adjust menu contents for "hide calculation"
        if (gasRecord.getMileage() != 0) {
            MenuItem itemHideCalc = menu.findItem(R.id.itemHideCalc);
            itemHideCalc.setChecked(gasRecord.isHidden());
        } else {
            menu.removeItem(R.id.itemHideCalc);
        }

    }

    /**
     * DESCRIPTION:
     * Called when a record in the list is clicked.
     *
     * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // display the context menu
        parent.showContextMenuForChild(view);
    }

    /**
     * DESCRIPTION:
     * Called when an item in a context menu is selected.
     *
     * @see android.app.Activity#onContextItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        // get index of currently selected row
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        selectedRow = (int) info.id;

        int itemId = item.getItemId();
        if (itemId == R.id.itemEdit) {
            editRow();
            return true;
        } else if (itemId == R.id.itemDelete) {
            showDialogFragment(DIALOG_CONFIRM_DELETE_ID);
            return true;
        } else if (itemId == R.id.itemShowEstimate) {
            DialogFragment dialogFragment = MileageEstimateDialogFragment.create(this, vehicle, records, selectedRow);
            dialogFragment.show(getSupportFragmentManager(), "mileageEstimateDialog");
            return true;
        } else if (itemId == R.id.itemShowCalc) {
            GasRecord gasRecord = records.get(selectedRow);
            DialogFragment dialogFragment = MileageCalculationDialogFragment.create(this, gasRecord);
            dialogFragment.show(getSupportFragmentManager(), "mileageCalcDialog");
            return true;
        } else if (itemId == R.id.itemHideCalc) {
            toggleHiddenCalculation(records.get(selectedRow));
            return true;
        }
        return super.onContextItemSelected(item);
    }

    /**
     * DESCRIPTION:
     * Toggle the "hidden calculation" attribute of the specified gas record.
     *
     * @param gasRecord - the gas record.
     */
    protected void toggleHiddenCalculation(GasRecord gasRecord) {
        boolean hidden = gasRecord.isHidden();
        gasRecord.setHidden(!hidden);
        var disposable = Observable.fromCallable(() ->
                gasRecordDao.update(gasRecord))
                .subscribeOn(Schedulers.io())
                .subscribe(i -> adapter.notifyDataSetChanged(),
                        e -> Utilities.toast(this, getString(R.string.toast_failed)));
        allDisposables.add(disposable);
        adapter.notifyDataSetChanged();
    }

    /**
     * DESCRIPTION:
     * Imports data from an ASCII CSV file into the log.
     */
    protected void importData(Uri uri) {

        InputStream file;
        try {
            file = getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            Utilities.toast(this, getString(R.string.toast_import_failed));
            return;
        }

        if (!gaslog.importData(vehicle, file)) {
            Utilities.toast(this, getString(R.string.toast_import_failed));
            return;
        }

        records.clear();
        var disposable = gasRecordDao.findAllByVehicle(vehicle.getId())
                .subscribeOn(Schedulers.io())
                .subscribe(allRecords -> {
                            records.addAll(allRecords);
                            adapter.notifyDataSetChanged();
                        },
                        Timber::e);
        allDisposables.add(disposable);
        Utilities.toast(this, getString(R.string.toast_import_complete));
    }

    /**
     * DESCRIPTION:
     * Select the name and path to a file for exporting log data.
     */
    protected void getExportFile() {
        String filename = vehicle.getName() + ".csv";
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/csv");
        intent.putExtra(Intent.EXTRA_TITLE, filename);
        startActivityForResult(intent, CHOOSE_EXPORT_FILE);
    }

    /**
     * DESCRIPTION:
     * Allow user to select a file to import using an installed cloud application.
     */
    private void showCloudStorageChooser() {
        try {
            Intent target = new Intent(Intent.ACTION_GET_CONTENT);
            // text/csv better, but need */* for google drive, else cannot select file
            target.setType("*/*");
            target.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(target, CHOOSE_IMPORT_FILE);
        } catch (android.content.ActivityNotFoundException ex) {
            Utilities.toast(this, getString(R.string.toast_activity_not_found));
        }
    }

    /**
     * DESCRIPTION:
     * Allow the user to select a file to import from internal storage.
     */
    private void showInternalStorageChooser() {

        if (ExternalStorage.isReadable()) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");

            startActivityForResult(intent, CHOOSE_IMPORT_FILE);
        } else {
            Utilities.toast(this, getString(R.string.toast_external_storage_not_readable));
        }

    }

    /**
     * DESCRIPTION:
     * Exports data from the log to an ASCII CSV file.
     */
    protected void exportData() {

        if (!ExternalStorage.isWritable()) {
            Utilities.toast(this, getString(R.string.toast_external_storage_not_writable));
            return;
        }

        getExportFile();
    }

    private void writeExportDataToFile(Uri file) {
        if (gaslog.exportData(vehicle, file)) {
            Utilities.toast(this, getString(R.string.toast_export_complete));
            Utilities.toast(this, file.toString());
        } else {
            Utilities.toast(this, getString(R.string.toast_export_failed));
        }
    }

    /**
     * DESCRIPTION:
     * Called when the user requests that a gasoline record be added to
     * the log. Starts a new Activity to allow the user to enter the data.
     */
    protected void getGas() {
        Intent intent = new Intent(this, GasRecordActivity.class);
        GasRecord gasRecord = new GasRecord();
        gasRecord.setVehicleID(vehicle.getId());
        var disposable = gasRecordDao.getCurrentOdometer(vehicle.getId())
                .subscribeOn(Schedulers.io())
                .subscribe(currentOdometer -> {
                    intent.putExtra(GasRecordActivity.RECORD, gasRecord);
                    intent.putExtra(GasRecordActivity.CURRENT_ODOMETER, currentOdometer);
                    intent.putExtra(GasRecordActivity.TANK_SIZE, vehicle.getTankSize());
                    startActivityForResult(intent, GET_GAS_REQUEST);
                },
                        Timber::e);
        allDisposables.add(disposable);
    }

    /**
     * DESCRIPTION:
     * Called when the user finishes entering data for a gasoline record
     * to add to the log.
     *
     * @param gasRecord - the GasRecord data entered by the user.
     */
    protected void onGetGasResult(GasRecord gasRecord) {

        var disposable = Observable.fromCallable(() ->
                gasRecordDao.insert(gasRecord))
                .subscribeOn(Schedulers.io())
                .subscribe(i -> {
                            Utilities.toast(this, getString(R.string.toast_data_saved));
                            records.add(gasRecord);
                            updateAfterNewRecord(gasRecord);
                        },
                        e -> Utilities.toast(this, getString(R.string.toast_error_saving_data)));
        allDisposables.add(disposable);

    }

    private void updateAfterNewRecord(GasRecord gasRecord) {
        boolean previousFullTank = records.stream().anyMatch(GasRecord::isFullTank);
        // recalculate mileage for the list
//        GasRecordList.calculateMileage(records, this);

        // notify adapter that the list has changed
        adapter.notifyDataSetChanged();

        // find the position of the record in the list
//        int position = GasRecordList.find(records, record);
        int position = records.size();

        // scroll that row into view
        listView.setSelection(position);

        // need a previous full tank in the log to do any calculations
        if (!previousFullTank) {
            DialogFragment dialogFragment = TankNeverFilledDialogFragment.create(this);
            dialogFragment.show(getSupportFragmentManager(), "tankNeverFilledDialog");
            return;
        }

        // display mileage calculation if possible
        if (MileageCalculationDialogFragment.isDisplayable(gasRecord)) {
            DialogFragment dialogFragment = MileageCalculationDialogFragment.create(this, gasRecord);
            dialogFragment.show(getSupportFragmentManager(), "mileageCalcDialog");
            return;
        }

        // display mileage estimate if possible
        if (MileageEstimateDialogFragment.isDisplayable(vehicle, records, position)) {
            DialogFragment dialogFragment = MileageEstimateDialogFragment.create(this, vehicle, records, position);
            dialogFragment.show(getSupportFragmentManager(), "mileageEstimateDialog");
        }

    }

    /**
     * DESCRIPTION:
     * Called when the user requests to edit a specific gasoline record.
     * Starts new Activity to allow the user to edit the data.
     */
    protected void editRow() {
        Intent intent = new Intent(this, GasRecordActivity.class);
        GasRecord gasRecord = records.get(selectedRow);
        intent.putExtra(GasRecordActivity.RECORD, gasRecord);
        intent.putExtra(GasRecordActivity.TANK_SIZE, vehicle.getTankSize());
        startActivityForResult(intent, EDIT_ROW_REQUEST);
    }

    /**
     * DESCRIPTION:
     * Called when the user finishes editing data for a gasoline record.
     *
     * @param gasRecord - the GasRecord data edited by the user.
     */
    protected void onEditRowResult(GasRecord gasRecord) {
        var disposable = Observable.fromCallable(() ->
                gasRecordDao.update(gasRecord))
                .subscribeOn(Schedulers.io())
                .subscribe(i -> {
                            records.set(selectedRow, gasRecord);
                            // todo fix
//                            GasRecordList.calculateMileage(records, this);
                            adapter.notifyDataSetChanged();
                        },
                        e -> Utilities.toast(this, getString(R.string.toast_edit_failed))
                );
        allDisposables.add(disposable);
    }

    /**
     * DESCRIPTION:
     * Called when the user has selected a gasoline record to delete
     * from the log and has confirmed deletion.
     */
    protected void deleteRow() {
        GasRecord gasRecord = records.get(selectedRow);
        var disposable = gasRecordDao.delete(gasRecord)
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                    // remove the record from our list of records
                    records.remove(selectedRow);

                    // re-calculate mileage based on the modified data
                    // TODO fix
//            GasRecordList.calculateMileage(records, this);

                    // update the list view
                    adapter.notifyDataSetChanged();
                }, e -> Utilities.toast(this, getString(R.string.toast_delete_failed)));
        allDisposables.add(disposable);
    }

    /**
     * DESCRIPTION:
     * Request code constants for onActivityResult()
     *
     * @see #onActivityResult(int, int, Intent)
     */
    private static final int EDIT_ROW_REQUEST = 1;
    private static final int GET_GAS_REQUEST = 2;
    private static final int CHOOSE_IMPORT_FILE = 3;
    private static final int CHOOSE_EXPORT_FILE = 4;

    /**
     * DESCRIPTION:
     * Called when an activity launched by this activity exits, giving the
     * requestCode it was started with, the resultCode it returned, and any
     * additional data from it.
     *
     * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        switch (requestCode) {
            case EDIT_ROW_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    GasRecord data = (GasRecord) intent.getSerializableExtra(GasRecordActivity.RECORD);
                    onEditRowResult(data);
                } else {
                    Utilities.toast(this, getString(R.string.toast_canceled));
                }
                break;

            case GET_GAS_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    GasRecord data = (GasRecord) intent.getSerializableExtra(GasRecordActivity.RECORD);
                    onGetGasResult(data);
                } else {
                    Utilities.toast(this, getString(R.string.toast_canceled));
                }
                break;

            case CHOOSE_IMPORT_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = intent.getData();
                    importData(uri);
                } else {
                    Utilities.toast(this, getString(R.string.toast_canceled));
                }

                break;

            case CHOOSE_EXPORT_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = intent.getData();
                    writeExportDataToFile(uri);
                } else {
                    Utilities.toast(this, getString(R.string.toast_canceled));
                }

                break;

            default:
                Utilities.toast(this, "Invalid request code.");
        }
    }

    /**
     * DESCRIPTION:
     * Dialog box integer ID constants
     *
     * @see #onCreateDialog(int)
     */
    protected static final int DIALOG_CONFIRM_DELETE_ID = 1;
    protected static final int DIALOG_CONFIRM_EXPORT_OVERWRITE_ID = 3;
    protected static final int DIALOG_CONFIRM_EXPORT_SHARE_ID = 7;

    /**
     * DESCRIPTION:
     * Called as needed by the framework to create dialog boxes used by the Activity.
     * Each dialog box is referenced by a locally defined id integer.
     *
     * @param id - the dialog id.
     */
    private void showDialogFragment(int id) {
        DialogFragment dialog;
        String title = "";
        String message = "";

        switch (id) {
            case DIALOG_CONFIRM_DELETE_ID:
                title = getString(R.string.title_confirm_log_delete_dialog);
                message = getString(R.string.message_confirm_log_delete_dialog);
                break;
            case DIALOG_CONFIRM_EXPORT_OVERWRITE_ID:
                title = getString(R.string.title_confirm_export_overwrite_dialog);
                message = getString(R.string.message_confirm_export_overwrite_dialog);
                message = String.format(message, vehicle.getName());
                break;
            case DIALOG_CONFIRM_EXPORT_SHARE_ID:
                title = getString(R.string.title_confirm_export_share_dialog);
                message = getString(R.string.message_confirm_export_share_dialog);
                break;
        }
        dialog = ConfirmationDialogFragment.create(this, this, id, title, message);
        dialog.show(getSupportFragmentManager(), String.valueOf(id));
    }

    /**
     * DESCRIPTION:
     * Called when a confirmation dialog gets a response from the user.
     *
     * @see ConfirmationDialogFragment.Listener#onConfirmationDialogResponse(int, boolean)
     */
    @Override
    public void onConfirmationDialogResponse(int id, boolean confirmed) {

        if (!confirmed) return;

        switch (id) {
            case DIALOG_CONFIRM_DELETE_ID:
                deleteRow();
                break;
            case DIALOG_CONFIRM_EXPORT_OVERWRITE_ID:
                exportData();
                break;
            case DIALOG_CONFIRM_EXPORT_SHARE_ID:
//                File file = getExportFile();
//                Intent intent = new Intent(Intent.ACTION_SEND);
//                intent.setType("text/plain");
//                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
//                intent.putExtra(Intent.EXTRA_SUBJECT, file.getName());
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//                startActivity(Intent.createChooser(intent, getString(R.string.title_chooser_share_csv)));
                break;

            default:
                Utilities.toast(this, "Invalid dialog id.");
        }

    }

    /**
     * DESCRIPTION:
     * Called when the user selects a file.
     * //	 * @see com.github.ch.tdussuet.fuellog.dialog.FileSelectionDialog.Listener#onFileSelectionDialogResponse(int, java.io.File)
     */
    @Override
    public void onStorageSelectionDialogResponse(StorageSelectionDialogFragment.Result result, String value) {
        if (result == StorageSelectionDialogFragment.Result.RESULT_CANCEL) {
            Utilities.toast(this, getString(R.string.toast_canceled));
            return;
        }

        if (value.equals("cloud")) {
            showCloudStorageChooser();
        } else {
            showInternalStorageChooser();
        }
    }

    /**
     * DESCRIPTION:
     * Updates the column header labels to the reflect the preferred
     * units for distance, liquid volume, etc.
     */
    protected void updateColumnHeaderLabels() {
        Units units = new Units(SettingsActivity.KEY_UNITS, this);
        TextView label = findViewById(R.id.headerGallons);
        label.setText(units.getLiquidVolumeLabel());
        label = findViewById(R.id.headerMileage);
        label.setText(units.getMileageLabel());
    }

    /**
     * DESCRIPTION:
     * Called when a shared preference value changes.
     *
     * @see android.content.SharedPreferences.OnSharedPreferenceChangeListener#onSharedPreferenceChanged(android.content.SharedPreferences, java.lang.String)
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsActivity.KEY_UNITS)) {

            // update the column header labels to reflect new units
            updateColumnHeaderLabels();

            // re-calculate mileage based on the new units
            // todo fix
//            GasRecordList.calculateMileage(records, this);
        }

        // update the list view
        adapter.notifyDataSetChanged();
    }

    /**
     * DESCRIPTION:
     * Save current state data.
     * NOTE: This gets called when the screen is rotated. The
     * Activity is then destroyed, re-created, and state restored.
     *
     * @see android.app.Activity#onSaveInstanceState(android.os.Bundle)
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("selectedRow", selectedRow);
    }

    /**
     * DESCRIPTION:
     * Restore previously saved state data.
     *
     * @see android.app.Activity#onRestoreInstanceState(android.os.Bundle)
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        selectedRow = savedInstanceState.getInt("selectedRow");
    }

}
