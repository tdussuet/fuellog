/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013,2014 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import ch.tdussuet.fuellog.App;
import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.dialog.ConfirmationDialogFragment;
import ch.tdussuet.fuellog.dialog.DataEntryModeDialogFragment;
import ch.tdussuet.fuellog.dialog.DataEntryModeDialogFragment.Result;
import ch.tdussuet.fuellog.item.GasRecordCalculator;
import ch.tdussuet.fuellog.item.GasRecordValidator;
import ch.tdussuet.fuellog.preferences.DataEntryMode;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.preferences.Units;
import ch.tdussuet.fuellog.ui.GasRecordWatcher;
import ch.tdussuet.fuellog.util.Utilities;
import ch.tdussuet.fuellog.util.format.CurrencyManager;

/**
 * DESCRIPTION:
 * Implements an Android Activity class to display one GasRecord and
 * allow the user to enter/edit values. The record is passed in/out
 * of the Activity via the Android Intent mechanism.
 */
public class GasRecordActivity extends FragmentActivity
        implements
        ConfirmationDialogFragment.Listener,
        DataEntryModeDialogFragment.Listener,
        View.OnFocusChangeListener {

    /// key name for the GasRecord to pass via Intent
    public static final String RECORD = GasRecordActivity.class.getName() + ".RECORD";

    /// key name for the current odometer value to pass via Intent
    public static final String CURRENT_ODOMETER = GasRecordActivity.class.getName() + ".CURRENT_ODOMETER";

    /// key name for the vehicle tank size value to pass via Intent
    public static final String TANK_SIZE = GasRecordActivity.class.getName() + ".TANK_SIZE";

    /// the GasRecord being edited
    private GasRecord gasRecord;

    /// the current odometer value for vehicle pertaining to the gas record
    private int currentOdometer;

    /// the tank size for vehicle pertaining to the gas record
    private float tankSize;

    /// the Activity's widgets (Views)
    private EditText editTextDate;
    private EditText editTextOdometer;
    private EditText editTextPrice;
    private EditText editTextCost;
    private EditText editTextGallons;
    private CheckBox checkBoxFullTank;
    private EditText editTextNotes;

    /// listens for EditText changes so that calculations can be refreshed
    private GasRecordWatcher watcher = null;

    /// the current data entry mode
    private DataEntryMode mode;

    public GasRecordActivity() {
        super();
    }

    /**
     * DECRIPTION:
     * Called when the activity is starting.
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {

            // get parameters from intent
            Intent intent = getIntent();
            gasRecord = (GasRecord) intent.getSerializableExtra(RECORD);
            currentOdometer = intent.getIntExtra(CURRENT_ODOMETER, -1);
            tankSize = intent.getFloatExtra(TANK_SIZE, 99999999f);
        } else {
            // restore the saved state
            gasRecord = (GasRecord) savedInstanceState.getSerializable("record");
            currentOdometer = savedInstanceState.getInt("current_odometer");
            tankSize = savedInstanceState.getFloat("tank_size");
        }
        loadForm();
    }

    /**
     * DESCRIPTION:
     * Loads and initializes a UI form (layout) based on the current data entry mode.
     */
    private void loadForm() {

        if (watcher != null) {
            watcher.destroy();
            watcher = null;
        }

        // load form layout for current data entry mode
        mode = new DataEntryMode(SettingsActivity.KEY_DATA_ENTRY_MODE, this);
        switch (mode.getValue()) {
            case DataEntryMode.CALCULATE_COST:
                setContentView(R.layout.activity_gas_record_calc_cost);
                break;
            case DataEntryMode.CALCULATE_GALLONS:
                setContentView(R.layout.activity_gas_record_calc_gallons);
                break;
            case DataEntryMode.CALCULATE_PRICE:
                setContentView(R.layout.activity_gas_record_calc_price);
                break;
        }

        // get view instances
        editTextDate = findViewById(R.id.editTextDate);
        editTextOdometer = findViewById(R.id.editTextOdometer);
        editTextPrice = findViewById(R.id.editTextPrice);
        editTextCost = findViewById(R.id.editTextCost);
        editTextGallons = findViewById(R.id.editTextGallons);
        checkBoxFullTank = findViewById(R.id.checkBoxFullTank);
        editTextNotes = findViewById(R.id.editTextNotes);

        // update labels to reflect current units
        Units units = new Units(SettingsActivity.KEY_UNITS, this);
        TextView label = findViewById(R.id.textViewOdometer);
        String format = getString(R.string.odometer_units_label);
        label.setText(String.format(App.getLocale(this), format, units.getDistanceLabelLowerCase()));
        label = findViewById(R.id.textViewGallons);
        format = getString(R.string.gasoline_label);
        label.setText(String.format(App.getLocale(this), format, units.getLiquidVolumeLabelLowerCase()));
        label = findViewById(R.id.textViewCost);
        format = getString(R.string.total_cost_label);
        label.setText(String.format(App.getLocale(this), format, CurrencyManager.getInstance(this).getCurrencySymbol()));
        label = findViewById(R.id.textViewPrice);
        format = getString(R.string.price_label);
        label.setText(String.format(App.getLocale(this), format, units.getLiquidVolumeRatioLabel()));

        // update hints to reflect current units
        editTextGallons.setHint(units.getLiquidVolumeLabelLowerCase());
        format = getString(R.string.hint_price);
        editTextPrice.setHint(String.format(App.getLocale(this), format, units.getLiquidVolumeRatioLabel()));

        // disallow newline in notes field
        editTextNotes.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                for (int i = s.length(); i > 0; i--) {
                    if (s.subSequence(i - 1, i).toString().equals("\n"))
                        s.replace(i - 1, i, "");
                }
            }
        });

        // copy data: record => form
        setData();

        // listen for changes to text values (so we can recalculate)
        watcher = createGasRecordWatcher();
        editTextPrice.setOnFocusChangeListener(this);
        editTextCost.setOnFocusChangeListener(this);
        editTextGallons.setOnFocusChangeListener(this);
    }

    /**
     * DESCRIPTION:
     * Saves the activity state before before screen rotation, etc.
     *
     * @see android.app.Activity#onSaveInstanceState(android.os.Bundle)
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putSerializable("record", gasRecord);
        savedInstanceState.putInt("current_odometer", currentOdometer);
        savedInstanceState.putFloat("tank_size", tankSize);
    }

    /**
     * DESCRIPTION:
     * Initialize the Activity's standard options menu.
     *
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.activity_gas_record, menu);
        //return true;
        return false;
    }

    /**
     * DESCRIPTION:
     * Sets the text displayed in the odometer EditText to
     * reflect the gas record value.
     */
    private void setOdometerText() {
        if (gasRecord.getOdometer() == 0) {
            editTextOdometer.setText("");
        } else {
            String value = String.valueOf(gasRecord.getOdometer());
            editTextOdometer.setText(value);
            editTextOdometer.setSelection(value.length());
        }
    }

    /**
     * DESCRIPTION:
     * Sets the text displayed in the price EditText to
     * reflect the gas record value.
     */
    private void setPriceText() {
        if (gasRecord.getCost() == 0) {
            editTextPrice.setText("");
        } else {
            String value = GasRecordValidator.getPriceString(this, gasRecord);
            editTextPrice.setText(value);
            editTextPrice.setSelection(value.length());
        }

    }

    /**
     * DESCRIPTION:
     * Sets the text displayed in the cost EditText to
     * reflect the gas record value.
     */
    private void setCostText() {
        if (gasRecord.getCost() == 0) {
            editTextCost.setText("");
        } else {
            String value = GasRecordValidator.getCostString(this, gasRecord);
            editTextCost.setText(value);
            editTextCost.setSelection(value.length());
        }
    }

    /**
     * DESCRIPTION:
     * Sets the text displayed in the gallons EditText to
     * reflect the gas record value.
     */
    private void setGallonsText() {
        if (gasRecord.getAmount() == 0) {
            editTextGallons.setText("");
        } else {
            String value = GasRecordValidator.getAmountString(this, gasRecord);
            editTextGallons.setText(value);
            editTextGallons.setSelection(value.length());
        }
    }

    /**
     * DESCRIPTION:
     * Sets the text displayed in the appropriate EditText to
     * reflect the calculated value.
     */
    private void setCalculatedText() {
        switch (mode.getValue()) {
            case DataEntryMode.CALCULATE_COST:
                setCostText();
                break;
            case DataEntryMode.CALCULATE_PRICE:
                setPriceText();
                break;
            case DataEntryMode.CALCULATE_GALLONS:
                setGallonsText();
                break;
        }

    }

    /**
     * DESCRIPTION:
     * Retrieves the text displayed in the odometer EditText and
     * stores the value in the gas record.
     */
    private boolean getOdometerText() {
        boolean valid = true;
        String value = editTextOdometer.getText().toString();
        try {
            gasRecord.setOdometer(GasRecordValidator.validateOdometer(value));
        } catch (NumberFormatException e) {
            valid = false;
        }
        return valid;
    }

    /**
     * DESCRIPTION:
     * Retrieves the text displayed in the cost EditText and
     * stores the value in the gas record.
     */
    private boolean getCostText() {
        boolean valid = true;
        String value = editTextCost.getText().toString().trim();
        try {
            gasRecord.setCost(GasRecordValidator.validateCost(value));
        } catch (NumberFormatException e) {
            valid = false;
        }

        if (!SettingsActivity.isCostRequired(this)) {
            valid = true;
        }

        return valid;
    }

    /**
     * DESCRIPTION:
     * Retrieves the text displayed in the price EditText and
     * stores the value in the gas record.
     */
    private boolean getPriceText() {
        boolean valid = true;
        String value = editTextPrice.getText().toString();
        try {
            gasRecord.setPrice(GasRecordValidator.validatePrice(value));
        } catch (NumberFormatException e) {
            valid = false;
        }
        return valid;
    }

    /**
     * DESCRIPTION:
     * Retrieves the text displayed in the gallons EditText and
     * stores the value in the gas record.
     */
    private boolean getGallonsText() {
        boolean valid = true;
        String value = editTextGallons.getText().toString();
        try {
            gasRecord.setAmount(GasRecordValidator.validateAmount(value));
        } catch (NumberFormatException e) {
            valid = false;
        }
        return valid;
    }

    /**
     * DESCRIPTION:
     * Calculate the cost, price, or gallons value based on
     * current data entry mode.
     *
     * @return false if calculation is not valid
     */
    private boolean getCalculatedValue() {
        boolean valid = true;
        try {
            switch (mode.getValue()) {
                case DataEntryMode.CALCULATE_COST:
                    var cost = GasRecordCalculator.calculateCost(gasRecord);
                    gasRecord.setCost(cost);
                    break;
                case DataEntryMode.CALCULATE_PRICE:
                    var price = GasRecordCalculator.calculatePrice(gasRecord);
                    gasRecord.setPrice(price);
                    break;
                case DataEntryMode.CALCULATE_GALLONS:
                    var amount = GasRecordCalculator.calculateAmount(gasRecord);
                    gasRecord.setAmount(amount);
                    break;
            }
        } catch (NumberFormatException e) {
            valid = false;
        }
        return valid;
    }

    /**
     * DESCRIPTION:
     * Indicate that a calculation error has occurred during data validation.
     */
    private void setCalculationError() {
        String message;
        switch (mode.getValue()) {
            case DataEntryMode.CALCULATE_COST:
                message = getString(R.string.toast_invalid_cost_calculation);
                editTextPrice.setError(message);
                editTextGallons.setError(message);
                break;
            case DataEntryMode.CALCULATE_PRICE:
                message = getString(R.string.toast_invalid_price_calculation);
                editTextCost.setError(message);
                editTextGallons.setError(message);
                break;
            case DataEntryMode.CALCULATE_GALLONS:
                message = getString(R.string.toast_invalid_gallons_calculation);
                editTextPrice.setError(message);
                editTextCost.setError(message);
                break;
        }
    }

    /**
     * DESCRIPTION:
     * Set the form values based on the GasRecord data values.
     */
    protected void setData() {

        editTextDate.setText(GasRecordValidator.getDateTimeString(this, gasRecord));

        setOdometerText();

        setPriceText();

        setCostText();

        setGallonsText();

        checkBoxFullTank.setChecked(gasRecord.isFullTank());

        editTextNotes.setText(gasRecord.getNotes());
    }

    /**
     * DESCRIPTION:
     * Get the current data values from the form after user edit,
     * validate them, and update the GasRecord being edited.
     *
     * @return boolean - indicates if form data is valid (valid=true)
     */
    protected boolean getData() {

        Units units = new Units(SettingsActivity.KEY_UNITS, this);
        String message;

        // reset any previous errors
        editTextOdometer.setError(null);
        editTextPrice.setError(null);
        editTextCost.setError(null);
        editTextGallons.setError(null);

        // odometer
        if (!getOdometerText()) {
            editTextOdometer.setError(getString(R.string.toast_invalid_odometer_value));
            editTextOdometer.requestFocus();
            return false;
        }

        // price
        if (!mode.isCalculatePrice() && !getPriceText()) {
            editTextPrice.setError(getString(R.string.toast_invalid_price_value));
            editTextPrice.requestFocus();
            return false;
        }

        // cost
        if (!mode.isCalculateCost() && !getCostText()) {
            editTextCost.setError(getString(R.string.toast_invalid_cost_value));
            editTextCost.requestFocus();
            return false;
        }

        // gallons
        if (!mode.isCalculateGallons() && !getGallonsText()) {
            message = getString(R.string.toast_invalid_gallons_value);
            message = String.format(message, units.getLiquidVolumeLabelLowerCase());
            editTextGallons.setError(message);
            editTextGallons.requestFocus();
            return false;
        }

        // get calculated value
        if (!getCalculatedValue()) {
            setCalculationError();
            return false;
        }

        // tank full
        gasRecord.setFullTank(checkBoxFullTank.isChecked());

        // if the tank is not full any more, reset the hidden calculation flag
        if (!gasRecord.isFullTank()) {
            gasRecord.setHidden(false);
        }

        // notes
        String value = editTextNotes.getText().toString();
        gasRecord.setNotes(value);

        // success - valid data set!
        return true;
    }

    /**
     * DESCRIPTION:
     * Evaluate the data and, if necessary, confirm that the user intended
     * to enter values that are in range but seem odd for the current
     * situation.
     *
     * @param id - the id of the last confirmation dialog displayed (0 = starting point)
     * @return true if all data values are acceptable/confirmed.
     */
    protected boolean confirmData(int id) {
        switch (id) {
            case 0: // starting point
                if (gasRecord.getOdometer() < currentOdometer) {
                    showDialogFragment(DIALOG_CONFIRM_ODOMETER_LOW_ID);
                    return false;
                }
                break;
            case DIALOG_CONFIRM_ODOMETER_LOW_ID:
                if (gasRecord.getAmount() > tankSize) {
                    showDialogFragment(DIALOG_CONFIRM_GALLONS_HIGH_ID);
                    return false;
                }
                break;
            default:
                return true;
        }
        return true;
    }

    /**
     * DESCRIPTION:
     * Called when the CANCEL button has been clicked.
     *
     * @param view the view
     */
    public void clickedCancel(View view) {
        returnResult(Activity.RESULT_CANCELED);
    }

    /**
     * DESCRIPTION:
     * Called when the OK button has been clicked (user is done editing data).
     *
     * @param view the view
     */
    public void clickedOk(View view) {

        hideSoftKeyboard();

        // validate the form data
        if (!getData()) return;

        // confirm values that are in range, but possibly wrong
        if (!confirmData(0)) return;

        // success
        returnResult(Activity.RESULT_OK);
    }

    /**
     * DESCRIPTION:
     * Called when the MODE button has been clicked to change
     * data entry mode.
     *
     * @param view the view
     */
    public void clickedMode(View view) {
        hideSoftKeyboard();
        DialogFragment dialogFragment = DataEntryModeDialogFragment.create(this, this, mode);
        dialogFragment.show(getSupportFragmentManager(), "dataEntryModeDialog");
    }

    /**
     * DESCRIPTION:
     * Called when user selects a data entry mode value from the dialog.
     *
     * @see DataEntryModeDialogFragment.Listener#onDataEntryModeDialogResponse(DataEntryModeDialogFragment.Result)
     */
    @Override
    public void onDataEntryModeDialogResponse(Result result) {

        if (result == Result.RESULT_CANCEL) return;

        // copy current data: form => record
        getOdometerText();
        if (!mode.isCalculateCost()) getCostText();
        if (!mode.isCalculatePrice()) getPriceText();
        if (!mode.isCalculateGallons()) getGallonsText();
        gasRecord.setFullTank(checkBoxFullTank.isChecked());
        gasRecord.setNotes(editTextNotes.getText().toString());

        // load form for selected mode
        loadForm();
    }


    /**
     * DESCRIPTION:
     * Called when the EDIT DATE button is clicked.
     *
     * @param view the view
     */
    public void clickedEditDate(View view) {
        // start an Activity to edit the date/time
        Intent intent = new Intent(this, DateTimeActivity.class);
        intent.putExtra(DateTimeActivity.MILLISECONDS, gasRecord.getDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        startActivityForResult(intent, EDIT_DATE_TIME_REQUEST);
    }

    /**
     * DESCRIPTION:
     * Called when a user response has been obtained from a confirmation dialog.
     *
     * @see ConfirmationDialogFragment.Listener#onConfirmationDialogResponse(int, boolean)
     */
    @Override
    public void onConfirmationDialogResponse(int id, boolean confirmed) {

        // continue confirming data
        if (confirmed && confirmData(id)) {
            returnResult(Activity.RESULT_OK);
        }
    }

    /**
     * DESCRIPTION:
     * Returns results the caller and closes this Activity.
     *
     * @param resultCode - the integer result code to return to caller.
     */
    protected void returnResult(int resultCode) {

        Intent intent = new Intent();

        // if successful edit, return edited gas record data to caller
        if (resultCode == Activity.RESULT_OK) {
            intent.putExtra(GasRecordActivity.RECORD, gasRecord);
        }

        setResult(resultCode, intent);
        finish();
    }

    /**
     * DESCRIPTION:
     * Dialog box integer ID constants
     *
     * @see #onCreateDialog(int)
     */
    protected static final int DIALOG_CONFIRM_ODOMETER_LOW_ID = 1;
    protected static final int DIALOG_CONFIRM_GALLONS_HIGH_ID = 2;

    /**
     * DESCRIPTION:
     * Called as needed by the framework to create dialog boxes used by the Activity.
     * Each dialog box is referenced by a locally defined id integer.
     */
    private void showDialogFragment(int id) {
        DialogFragment dialog;
        String title = "";
        String message = "";
        String value;

        Units units = new Units(SettingsActivity.KEY_UNITS, this);

        switch (id) {
            case DIALOG_CONFIRM_ODOMETER_LOW_ID:
                title = getString(R.string.title_confirm_odometer);
                message = getString(R.string.message_confirm_odometer);
                value = Integer.toString(currentOdometer);
                message = String.format(message, value);
                break;

            case DIALOG_CONFIRM_GALLONS_HIGH_ID:
                title = getString(R.string.title_confirm_gallons);
                title = String.format(title, units.getLiquidVolumeLabel());
                message = getString(R.string.message_confirm_gallons);
                value = String.format(App.getLocale(this), "%.1f %s",
                        tankSize, units.getLiquidVolumeLabelLowerCase());
                message = String.format(message, value);
                break;
        }
        dialog = ConfirmationDialogFragment.create(this, this, id, title, message);
        dialog.show(getSupportFragmentManager(), String.valueOf(id));
    }

    /**
     * DESCRIPTION:
     * Request code constants for onActivityResult()
     *
     * @see #onActivityResult(int, int, Intent)
     */
    private static final int EDIT_DATE_TIME_REQUEST = 1;

    /**
     * DESCRIPTION:
     * Called when an activity launched by this activity exits, giving the
     * requestCode it was started with, the resultCode it returned, and any
     * additional data from it.
     *
     * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == EDIT_DATE_TIME_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                long milliseconds = intent.getLongExtra(DateTimeActivity.MILLISECONDS, -1);
                if (milliseconds > 0) {
                    LocalDateTime date = Instant.ofEpochMilli(milliseconds).atZone(ZoneId.systemDefault()).toLocalDateTime();
                    gasRecord.setDate(date);
                    editTextDate.setText(GasRecordValidator.getDateTimeString(this, gasRecord));
                }
            }
        } else {
            Utilities.toast(this, "Invalid Request Code.");
        }
    }

    /**
     * DESCRIPTION:
     * Called when the focus state of a view has changed.
     *
     * @see android.view.View.OnFocusChangeListener#onFocusChange(android.view.View, boolean)
     */
    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (!hasFocus) {
            // losing focus...reformat the displayed text if it is valid
            int id = view.getId();
            if (id == R.id.editTextPrice) {
                if (getPriceText()) setPriceText();
            } else if (id == R.id.editTextCost) {
                if (getCostText()) setCostText();
            } else if (id == R.id.editTextGallons) {
                if (getGallonsText()) setGallonsText();
            }
        }

    }

    /**
     * DESCRIPTION:
     * Creates a container for TextWatcher instances that listen for and handle changes for the
     * EditText fields.
     *
     * @return TextWatcher
     */
    private GasRecordWatcher createGasRecordWatcher() {
        return new GasRecordWatcher(mode, editTextPrice, editTextCost, editTextGallons) {
            public void priceChanged() {
                getPriceText();
                getCalculatedValue();
                setCalculatedText();
            }

            public void costChanged() {
                getCostText();
                getCalculatedValue();
                setCalculatedText();
            }

            public void gallonsChanged() {
                getGallonsText();
                getCalculatedValue();
                setCalculatedText();
            }

        };
    }

    /**
     * DESCRIPTION:
     * Dismisses the soft keyboard.
     */
    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(editTextOdometer.getWindowToken(), 0);
        }
    }

}
