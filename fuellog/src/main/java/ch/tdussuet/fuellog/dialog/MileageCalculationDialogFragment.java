/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.preferences.Units;
import ch.tdussuet.fuellog.ui.GasGauge;
import ch.tdussuet.fuellog.util.MileageCalculation;

/**
 * DESCRIPTION:
 * A dialog box to display mileage calculation information.
 */
public class MileageCalculationDialogFragment extends DialogFragment {

    /// the mileage calculation to display
    private MileageCalculation calculation;
    private Activity activity;

    /**
     * DESCRIPTION:
     * Determines if the dialog can be displayed for a specific set of data.<p>
     * Data requirements:
     * <ul>
     * <li>The record must have a MileageCalculation instance.
     * </ul>
     *
     * @param gasRecord - the gas record to display mileage calculation for.
     * @return true if the dialog can be displayed, false otherwise.
     */
    public static boolean isDisplayable(GasRecord gasRecord) {
        return true; // TODO fix
    }

    public MileageCalculationDialogFragment() {
    }

    public void setCalculation(MileageCalculation calculation) {
        this.calculation = calculation;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    /**
     * DESCRIPTION:
     * Creates an instance of the dialog.
     *
     * @param activity - the activity creating the dialog.
     * @param gasRecord   - the gas record to be used for the calculation.
     * @return - the Dialog
     */
    public static DialogFragment create(final Activity activity, GasRecord gasRecord) {
        MileageCalculationDialogFragment dialogFragment = new MileageCalculationDialogFragment();
        dialogFragment.setActivity(activity);
        // TODO fix
//        dialogFragment.setCalculation(gasRecord.getCalculation());
        dialogFragment.setArguments(new Bundle());
        return dialogFragment;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Context context = activity;
        Resources res = context.getResources();

        // create a custom dialog instance
        final Dialog dialog = new Dialog(activity, R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_mileage_calculation);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);  // via back key

        // get current units of measurement
        Units units = new Units(SettingsActivity.KEY_UNITS, context);

        // format the dialog title string
        String title = String.format(
                res.getString(R.string.title_mileage_calculation),
                units.getMileageLabel());

        // display the title
        TextView textTitle = dialog.findViewById(R.id.textTitle);
        textTitle.setText(title);

        // display the calculation text
        TextView textCalculation = dialog.findViewById(R.id.textCalculation);
        textCalculation.setText(getCalculationText(calculation));

        // remove the note text (used only for estimates)
        TextView textNote = dialog.findViewById(R.id.textNote);
        textNote.setText(null);

        // initialize gauge to reflect full tank
        GasGauge viewGauge = dialog.findViewById(R.id.viewGauge);
        viewGauge.setHandTarget(1.0f);
        viewGauge.setInteractive(false);

        // remove the dialog when the close image is clicked
        ImageView imageCloseDialog = dialog.findViewById(R.id.imageCloseDialog);
        imageCloseDialog.setOnClickListener(v -> dialog.dismiss());

        // remove the dialog when canceled (via back key)
        dialog.setOnCancelListener(DialogInterface::dismiss);

        return dialog;
    }

    /**
     * DESCRIPTION:
     * Creates a message String describing a specific mileage calculation.
     *
     * @param calc - the MileageCalculation.
     * @return a String describing the calculation.
     */
    private String getCalculationText(MileageCalculation calc) {
        Resources res = activity.getResources();

        if (calc == null) {
            return res.getString(R.string.mileage_calculation_none);
        }

        Units units = calc.getUnits();

        return String.format(res.getString(R.string.mileage_calculation_drove),
                calc.getDistanceDriven(),
                units.getDistanceLabelLowerCase()) +
                String.format(res.getString(R.string.mileage_calculation_used),
                        calc.getGasolineUsedString(),
                        units.getLiquidVolumeLabelLowerCase()) +
                String.format("%s %s",
                        calc.getMileageString(),
                        units.getMileageLabel());
    }


}
