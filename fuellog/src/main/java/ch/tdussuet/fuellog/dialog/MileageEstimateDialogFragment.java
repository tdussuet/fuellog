/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.util.List;

import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.db.entity.Vehicle;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.preferences.Units;
import ch.tdussuet.fuellog.ui.CheatSheet;
import ch.tdussuet.fuellog.ui.CheatSheet.Trigger;
import ch.tdussuet.fuellog.ui.GasGauge;
import ch.tdussuet.fuellog.util.MileageCalculation;

/**
 * DESCRIPTION:
 * A dialog box to calculate and display mileage estimates based
 * on gas gauge hand position.
 */
public class MileageEstimateDialogFragment extends DialogFragment {

    /// the vehicle that the estimate is for
    private Vehicle vehicle;

    /// current list of gas records from the log for estimation purposes
    private List<GasRecord> records;

    // the estimate
    private MileageCalculation calculation;

    private Activity activity;

    /**
     * DESCRIPTION:
     * Determines if the dialog can be displayed for a specific set of data.<p>
     * Data requirements:
     * <ul>
     * <li>The list must be sorted by odometer value.
     * <li>The list must contain at least two gas records:<ol>
     * <li>the record being evaluated (cannot be full tank).
     * <li>a previous record with full tank.
     * </ol>
     * </ul>
     *
     * @param vehicle  - the Vehicle to calculate estimates for.
     * @param records  - a List of gas records for the vehicle.
     * @param location - the index of the gas record in the list to estimate mileage for.
     * @return true if the dialog can be displayed, false otherwise.
     */
    public static boolean isDisplayable(Vehicle vehicle, List<GasRecord> records, int location) {

        // need valid vehicle tank size for estimates
        if ((vehicle == null) || (vehicle.getTankSize() <= 0.0f)) {
            return false;
        }

        // need at least 2 gas records...the record being evaluated and a previous fill up
        if ((records == null) || (records.size() < 2)) {
            return false;
        }

        // range check the specified gas record location
        if ((location < 1) || (location > (records.size() - 1))) {
            return false;
        }

        // the record being evaluated should not be a full tank
        if (records.get(location).isFullTank()) {
            return false;
        }

        // a previous fill up must exist
        return records.stream().limit(location).anyMatch(GasRecord::isFullTank);

        // can display the dialog for the specified data!
    }

    public MileageEstimateDialogFragment() {
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public void setRecords(List<GasRecord> records) {
        this.records = records;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    /**
     * DESCRIPTION:
     * Creates an instance of the dialog. Uses the data previously specified
     * via the init() method.
     *
     * @param activity - the activity creating the dialog.
     * @param vehicle  - the vehicle
     * @param records  - the gas records
     * @param location - the index of the gas record in the list to estimate mileage for.
     * @return - the Dialog instance.
     */
    public static DialogFragment create(final Activity activity, Vehicle vehicle, List<GasRecord> records, int location) {
        MileageEstimateDialogFragment dialogFragment = new MileageEstimateDialogFragment();
        dialogFragment.setActivity(activity);
        dialogFragment.setVehicle(vehicle);
        // copy the records we need from the list
        // - the first record in the list is the previous full tank
        // - the last record in the list is the record being evaluated
        // TODO fix
//        int fullTank = GasRecordList.findPreviousFullTank(records, location);
//        dialogFragment.setRecords(GasRecordList.subList(records, fullTank, location + 1));
        dialogFragment.setArguments(new Bundle());
        return dialogFragment;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = activity;
        Resources res = context.getResources();

        // create a custom dialog instance
        final Dialog dialog = new Dialog(activity, R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_mileage_calculation);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);  // via back key

        // get current units of measurement
        Units units = new Units(SettingsActivity.KEY_UNITS, context);

        // format the dialog title string
        String title = String.format(
                res.getString(R.string.title_mileage_estimate),
                units.getMileageLabel());

        // display the title
        TextView textTitle = dialog.findViewById(R.id.textTitle);
        textTitle.setText(Html.fromHtml(title + "<sup><small>*</small></sup>", Html.FROM_HTML_MODE_LEGACY));

        // display the estimated calculation (or help info if not calculated yet)
        TextView textCalculation = dialog.findViewById(R.id.textCalculation);
        String estimate = res.getString(R.string.mileage_estimate_initial);
        if (calculation != null) {
            estimate = getEstimateString(calculation, context);
        }
        textCalculation.setText(estimate);

        // initialize gauge to reflect 1/2 tank
        GasGauge viewGauge = dialog.findViewById(R.id.viewGauge);
        viewGauge.setHandTarget(0.5f);
        viewGauge.setInteractive(true);

        // update the estimate when the gauge hand position changes
        viewGauge.setOnHandPositionChangedListener((source, handPosition) -> {
            calculation = getEstimateCalculation(handPosition, context);
            TextView textCalculation1 = dialog.findViewById(R.id.textCalculation);
            textCalculation1.setText(getEstimateString(calculation, context));
        });

        // add note indicating that the estimate is based on vehicle tank size
        TextView textNote = dialog.findViewById(R.id.textNote);
        if (vehicle == null) {
            textNote.setText(null);
        } else {
            // construct the note message string
            String note = String.format(
                    res.getString(R.string.mileage_estimate_note),
                    String.valueOf(vehicle.getTankSize()),
                    units.getLiquidVolumeLabelLowerCase());
            textNote.setText(Html.fromHtml("<sup><small>*</small></sup><u>" + note + "</u>", Html.FROM_HTML_MODE_LEGACY));

            // add "tool tip" to the note explaining how to specify tank size
            CheatSheet.setup(textNote, R.string.mileage_estimate_tanksize_info, Trigger.Click);
        }

        // remove the dialog when the close image is clicked
        ImageView image = dialog.findViewById(R.id.imageCloseDialog);
        image.setOnClickListener(v -> dialog.dismiss());

        // remove the dialog when canceled (via back key)
        dialog.setOnCancelListener(DialogInterface::dismiss);

        // return the dialog
        return dialog;
    }

    /**
     * DESCRIPTION:
     * Creates a message String describing a specific mileage calculation.
     *
     * @param calc - the MileageCalculation.
     * @return a String describing the calculation.
     */
    private String getEstimateString(MileageCalculation calc, Context context) {

        Resources res = context.getResources();

        if (calc == null) {
            return res.getString(R.string.mileage_estimate_none);
        }

        Units units = calc.getUnits();

        return String.format(res.getString(R.string.mileage_estimate_drove),
                calc.getDistanceDriven(),
                units.getDistanceLabelLowerCase()) +
                String.format(res.getString(R.string.mileage_estimate_used),
                        calc.getGasolineUsedString(),
                        units.getLiquidVolumeLabelLowerCase()) +
                String.format("%s %s",
                        calc.getMileageString(),
                        units.getMileageLabel());
    }

    /**
     * DESCRIPTION:
     * Calculates estimated mileage based on a specified gas gauge hand position.
     *
     * @param position - the gas gauge hand position (0.0 [empty] - 1.0 [full])
     * @return the estimated MileageCalculation (null = no calculation).
     */
    private MileageCalculation getEstimateCalculation(float position, Context context) {

        // safety net - necessary data should have been initialized before getting here!
        // returning null will result in display of "no calculation" message
        if ((vehicle == null) || (records == null) || (records.size() < 2)) {
            return null;
        }

        // calculate how much gas is needed to fill the tank based on current gauge position
        float filltank = vehicle.getTankSize() * (1.0f - position);

        // get the record at the end of the list
        int end = records.size() - 1;
        GasRecord gasRecord = records.get(end);

        // create an estimate reflecting the record with a full tank
        // TODO fix
//        GasRecord estimate = new GasRecord(gasRecord);
//        estimate.setGallons(gasRecord.getGallons() + filltank);
//        estimate.setFullTank(true);

        // replace saved record with the estimate at the end of the list
//        records.set(end, estimate);

        // calculate estimated mileage
//        GasRecordList.calculateMileage(records, context);

        // restore the real record to the list
        records.set(end, gasRecord);

        // return the estimated mileage
//        return estimate.getCalculation();
        return null;
    }

}
