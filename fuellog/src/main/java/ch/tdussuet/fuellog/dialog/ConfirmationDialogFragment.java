/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import ch.tdussuet.fuellog.R;

/**
 * DESCRIPTION:
 * A general purpose dialog to use for confirmation of program actions.
 */
public class ConfirmationDialogFragment extends DialogFragment {

    private ConfirmationDialogFragment.Listener listener;
    private Context context;

    /**
     * DESCRIPTION:
     * The activity that creates an instance of this dialog must
     * implement this interface in order to receive event callbacks.
     */
    public interface Listener {
        /**
         * DESCRIPTION:
         * Called when the dialog closes to report the response to the listener.
         *
         * @param id        - the id value specified when the dialog was created.
         * @param confirmed - boolean indicating result (true = confirmed).
         */
        void onConfirmationDialogResponse(int id, boolean confirmed);
    }

    public ConfirmationDialogFragment() {
    }

    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private static final String DIALOG_ID = "dialog_id";

    /**
     * DESCRIPTION:
     * Creates an instance of the dialog.
     *
     * @param context  - the Context of the activity/application creating the dialog.
     * @param listener - a Listener to notify of dialog events.
     * @param id       - the dialog id
     * @param title    - the title String to display.
     * @param message  - the message String to display.
     * @return - the Dialog.
     */
    public static ConfirmationDialogFragment create(
            Context context,
            final Listener listener,
            final int id,
            String title,
            String message) {

        ConfirmationDialogFragment dialogFragment = new ConfirmationDialogFragment();
        dialogFragment.setListener(listener);
        dialogFragment.setContext(context);
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(MESSAGE, message);
        args.putInt(DIALOG_ID, id);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    private void setContext(Context context) {
        this.context = context;
    }

    private void setListener(ConfirmationDialogFragment.Listener listener) {
        this.listener = listener;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Resources res = context.getResources();

        String title = "";
        String message = "";
        int id = 0;
        if (savedInstanceState != null) {
            title = savedInstanceState.getString(TITLE);
            message = savedInstanceState.getString(MESSAGE);
            id = savedInstanceState.getInt(DIALOG_ID);
        } else if (this.getArguments() != null) {
            Bundle arguments = this.getArguments();
            title = arguments.getString(TITLE);
            message = arguments.getString(MESSAGE);
            id = arguments.getInt(DIALOG_ID);
        }
        // Build the dialog and set up the button click handlers
        String yes_label = res.getString(R.string.yes_label);
        String no_label = res.getString(R.string.no_label);
        Drawable icon = ContextCompat.getDrawable(context, R.drawable.ic_dialog_alert);
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogTheme);
        final int dialog_id = id;
        builder.setMessage(message)
                .setTitle(title)
                .setIcon(icon)
                .setPositiveButton(yes_label, (dialog, which) -> listener.onConfirmationDialogResponse(dialog_id, true))
                .setNegativeButton(no_label, (dialog, which) -> listener.onConfirmationDialogResponse(dialog_id, false));

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

}
