/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import java.util.List;

import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.preferences.Units;
import ch.tdussuet.fuellog.ui.GasGauge;

/**
 * DESCRIPTION:
 * A dialog box to inform the user that mileage calculations cannot be performed
 * until a full tank is logged.
 */
public class TankNeverFilledDialogFragment extends DialogFragment {

    private Activity activity;

    /**
     * DESCRIPTION:
     * Determines if the dialog can be displayed for a specific set of data.
     *
     * @param records - the gas records to evaluate.
     * @return true if the dialog can be displayed, false otherwise.
     */
    public static boolean isDisplayable(List<GasRecord> records) {
        return records.stream().anyMatch(GasRecord::isFullTank);
    }

    public TankNeverFilledDialogFragment() {
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    /**
     * DESCRIPTION:
     * Creates an instance of the dialog.
     *
     * @param activity - the activity creating the dialog.
     * @return - the Dialog
     */
    public static TankNeverFilledDialogFragment create(final Activity activity) {
        TankNeverFilledDialogFragment dialogFragment = new TankNeverFilledDialogFragment();
        dialogFragment.setActivity(activity);
        dialogFragment.setArguments(new Bundle());
        return dialogFragment;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = activity;
        Resources res = context.getResources();

        // create a custom dialog instance
        final Dialog dialog = new Dialog(activity, R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_mileage_calculation);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);  // via back key

        // get current units of measurement
        Units units = new Units(SettingsActivity.KEY_UNITS, context);

        // format the dialog title string
        String title = String.format(
                res.getString(R.string.title_mileage_calculation),
                units.getMileageLabel());

        // display the title
        TextView textTitle = dialog.findViewById(R.id.textTitle);
        textTitle.setText(title);

        // display the text explaining why we cannot calculate mileage
        TextView textCalculation = dialog.findViewById(R.id.textCalculation);
        textCalculation.setText(res.getString(R.string.message_tank_never_filled));

        // get the gas gauge view from the layout
        GasGauge viewGauge = dialog.findViewById(R.id.viewGauge);

        // create an image of a gas gauge with same layout parameters
        ImageView imageGauge = new ImageView(dialog.getContext());
        Drawable icon = ContextCompat.getDrawable(context, R.drawable.gauge_background_question);
        imageGauge.setImageDrawable(icon);
        imageGauge.setLayoutParams(viewGauge.getLayoutParams());

        // replace gas gauge view with the image
        ViewGroup parent = (ViewGroup) viewGauge.getParent();
        int index = parent.indexOfChild(viewGauge);
        parent.removeView(viewGauge);
        parent.addView(imageGauge, index);

        // remove the note text (not needed)
        TextView textNote = dialog.findViewById(R.id.textNote);
        textNote.setText(null);

        // remove the dialog when the close image is clicked
        ImageView imageCloseDialog = dialog.findViewById(R.id.imageCloseDialog);
        imageCloseDialog.setOnClickListener(v -> dialog.dismiss());
        dialog.setOnCancelListener(DialogInterface::dismiss);

        // return the dialog
        return dialog;
    }

}
