/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.dialog;

import android.app.Dialog;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import ch.tdussuet.fuellog.R;

/**
 * DESCRIPTION:
 * A dialog allowing a user to select preferred units of measurement
 * from a list of possible values.
 */
public class UnitsDialogFragment extends DialogFragment {

    private Listener listener;
    private Context context;

    /// a result code to be returned to the listener
    public enum Result {RESULT_SELECTED, RESULT_CANCEL}

    /**
     * DESCRIPTION:
     * The activity that creates an instance of this dialog must
     * implement this interface in order to receive event callbacks.
     */
    public interface Listener {
        /**
         * DESCRIPTION:
         * Called when the dialog closes to report the response to the listener.
         *
         * @param result - whether the dialog was canceled or is returning successfully
         * @param value  - the selected Units value (null = no selection).
         */
        void onUnitsDialogResponse(Result result, String value);
    }

    public UnitsDialogFragment() {
    }

    /**
     * DESCRIPTION:
     * Creates an instance of the dialog.
     *
     * @param listener - a Listener to notify of dialog events.
     * @return - the Dialog.
     */
    public static UnitsDialogFragment create(final Listener listener, final Context context) {
        UnitsDialogFragment dialogFragment = new UnitsDialogFragment();
        dialogFragment.setListener(listener);
        dialogFragment.setContext(context);
        dialogFragment.setArguments(new Bundle());
        return dialogFragment;
    }

    private void setContext(Context context) {
        this.context = context;
    }

    private void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Resources res = context.getResources();

        Drawable icon = ContextCompat.getDrawable(context, R.drawable.ic_dialog_menu_generic);
        // Build the dialog and set up the click handlers
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogTheme);
        builder
                .setTitle(R.string.title_units_dialog)
                .setIcon(icon)
                .setItems(R.array.arrayUnitsEntries, (dialog, which) -> {
                    String[] values = res.getStringArray(R.array.arrayUnitsEntryValues);
                    listener.onUnitsDialogResponse(Result.RESULT_SELECTED, values[which]);
                });

        // require user selection
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        dialog.setOnCancelListener(dialog1 -> listener.onUnitsDialogResponse(Result.RESULT_CANCEL, null));

        // return the dialog
        return dialog;
    }

}
