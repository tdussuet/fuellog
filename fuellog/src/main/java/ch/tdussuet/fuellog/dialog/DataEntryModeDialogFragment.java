/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2014 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.preferences.DataEntryMode;
import ch.tdussuet.fuellog.preferences.SettingsActivity;

/**
 * DESCRIPTION:
 * A dialog allowing a user to select preferred data entry mode.
 */
public class DataEntryModeDialogFragment extends DialogFragment {

    protected static final String TAG = DataEntryModeDialogFragment.class.getName();

    /// a result code to be returned to the listener
    public enum Result {RESULT_SELECTED, RESULT_CANCEL}

    private Context context;
    private Listener listener;
    private DataEntryMode dataEntryMode;

    /**
     * DESCRIPTION:
     * The activity that creates an instance of this dialog must
     * implement this interface in order to receive event callbacks.
     */
    public interface Listener {
        /**
         * DESCRIPTION:
         * Called when the dialog closes to report the response to the listener.
         *
         * @param result - the result of the action
         */
        void onDataEntryModeDialogResponse(Result result);
    }

    public DataEntryModeDialogFragment() {
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setDataEntryMode(DataEntryMode dataEntryMode) {
        this.dataEntryMode = dataEntryMode;
    }

    /**
     * DESCRIPTION:
     * Creates an instance of the dialog.
     *
     * @param context  - the Context of the activity/application creating the dialog.
     * @param listener - a Listener to notify of dialog events.
     * @param mode     - the current DataEntryMode
     * @return - the Dialog.
     */
    public static DialogFragment create(Context context, final Listener listener, final DataEntryMode mode) {
        DataEntryModeDialogFragment dialogFragment = new DataEntryModeDialogFragment();
        dialogFragment.setContext(context);
        dialogFragment.setListener(listener);
        dialogFragment.setDataEntryMode(mode);
        dialogFragment.setArguments(new Bundle());
        return dialogFragment;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Resources res = context.getResources();

        Dialog dialog;

        // Build the dialog and set up the click handlers
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogTheme);
        Drawable icon = ContextCompat.getDrawable(context, R.drawable.ic_dialog_menu_generic);
        builder
                .setTitle(R.string.title_data_entry_mode_dialog)
                .setIcon(icon)
                .setSingleChoiceItems(R.array.arrayDataEntryModeEntries, dataEntryMode.getValue(), (dialog1, which) -> {
                    String[] values = res.getStringArray(R.array.arrayDataEntryModeValues);
                    if ((which < 0) || (which >= values.length)) which = 0;
                    SettingsActivity.setString(dataEntryMode.getKey(), values[which], context);
                    listener.onDataEntryModeDialogResponse(Result.RESULT_SELECTED);
                    dialog1.dismiss();
                });

        // require user selection
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        dialog.setOnCancelListener(dialog12 -> listener.onDataEntryModeDialogResponse(Result.RESULT_CANCEL));

        // return the dialog
        return dialog;
    }

}
