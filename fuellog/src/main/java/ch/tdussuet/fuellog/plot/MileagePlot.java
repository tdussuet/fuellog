/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.plot;

import android.text.format.DateFormat;

import androidx.annotation.NonNull;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYRegionFormatter;
import com.androidplot.xy.XYSeriesFormatter;

import java.text.DecimalFormat;
import java.text.Format;
import java.util.LinkedList;
import java.util.List;

import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.activity.PlotActivity;
import ch.tdussuet.fuellog.db.converter.DateTimeConverter;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.item.GasRecordValidator;
import ch.tdussuet.fuellog.preferences.PlotDateRange;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import lombok.val;
import timber.log.Timber;

/**
 * DESCRIPTION:
 * A plot of gas mileage data.
 */
public class MileagePlot extends UnitBasedPlot {

    private static final String TAG = MileagePlot.class.getName();

    private PlotDateRange range;

    @Override
    protected void setupSpecificPlot() {
        range = new PlotDateRange(activity, SettingsActivity.KEY_PLOT_DATE_RANGE);

        plot.setLinesPerRangeLabel(2);
        plot.setLinesPerDomainLabel(4);
    }

    @NonNull
    @Override
    protected XYSeriesFormatter<XYRegionFormatter> getPlotFormatter() {
        val plotFormatter = new LineAndPointFormatter(
                activity.getResources().getColor(R.color.plot_line_color, activity.getTheme()),
                activity.getResources().getColor(R.color.plot_point_color, activity.getTheme()),
                activity.getResources().getColor(R.color.plot_fill_color, activity.getTheme()),
                null);

        // use hairline width line for plotting (to ease reading avg label)
        plotFormatter.getLinePaint().setStrokeWidth(0);
        return plotFormatter;
    }

    @Override
    protected Format getXlabels() {
        return DateFormat.getDateFormat(activity);
    }

    @Override
    protected Format getYlabels() {
        return new DecimalFormat("###0.0");
    }

    @Override
    protected String getPlotXLabel() {
        return "";
    }

    @NonNull
    @Override
    protected String getPlotYLabel() {
        return units.getMileageLabel();
    }

    /**
     * DESCRIPTION:
     * Sets the boundaries for the X and Y-axis based on the data values.
     */
    @Override
    protected void setPlotAxisBoundaries() {

        final String tag = TAG + ".setPlotAxsBoundaries()";

        final long MSEC_PER_DAY = 86400000L;

        // calculate and set y-axis boundaries
        double pad = (maxy - miny) * 0.2d;
        if (pad == 0) pad = 0.5d;
        double upperboundy = Math.ceil(maxy + pad);
        double lowerboundy = Math.floor(miny - pad);
        if (lowerboundy < 0d) lowerboundy = 0d;
        Timber.d(tag, "lowerboundy=" + lowerboundy + " upperboundy=" + upperboundy);
        plot.setRangeBoundaries(lowerboundy, upperboundy, BoundaryMode.FIXED);

        // calculate and set y-axis step size
        double stepy = 0.25d;
        double rangey = upperboundy - lowerboundy;
        while (rangey / stepy > 20.0f) stepy *= 2;
        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, stepy);

        // calculate x-axis boundaries
        lowerboundx = DateTimeConverter.fromDate(range.getStartDate());
        upperboundx = DateTimeConverter.fromDate(range.getEndDate());
        if (range.getDateRange() == PlotDateRange.ALL) {
            lowerboundx = minx;
        }

        // special case: one data point - expand range to center it in the plot
        if ((average != 0) && (lowerboundx == upperboundx)) {
            lowerboundx -= MSEC_PER_DAY;
            upperboundx += MSEC_PER_DAY;
        }

        // set x-axis boundaries
        Timber.d(tag, "lowerboundx=" + lowerboundx + " upperboundx=" + upperboundx);
        plot.setDomainBoundaries(lowerboundx, upperboundx, BoundaryMode.FIXED);
        plot.setLinesPerDomainLabel(4);
    }

    /**
     * DESCRIPTION:
     * Obtains (x,y) values from the current data set for plotting. Also
     * calculates the average y-value for the series.
     *
     * @return a SimpleXYSeries instance containing (x,y) values to plot.
     */
    @Override
    protected SimpleXYSeries getPlotSeries() {

        final String tag = TAG + ".getPlotSeries()";

        // get plot date range from preferences
        range = new PlotDateRange(activity, SettingsActivity.KEY_PLOT_DATE_RANGE);

        // create lists of x-axis, and y-axis numbers to plot
        List<Number> xNumbers = new LinkedList<>();
        List<Number> yNumbers = new LinkedList<>();

        // get numbers to plot from gas record data, where (x,y) is:
        // x = time in milliseconds (from date) plus an index to avoid duplicate values
        // y = calculated mileage at that date
        float sumy = 0;
        miny = Float.MAX_VALUE;
        maxy = Float.MIN_VALUE;
        minx = Long.MAX_VALUE;
        maxx = Long.MIN_VALUE;
        for (GasRecord gasRecord : ((PlotActivity)activity).getRecords()) {
            if (gasRecord.getMileage() != 0 &&
                    !gasRecord.isHidden() &&
                    range.contains(gasRecord.getDate())) {

                long x = DateTimeConverter.fromDate(gasRecord.getDate()) + xNumbers.size();
                float y = gasRecord.getMileage();
                Timber.d(tag, "date=" + GasRecordValidator.getDateTimeString(activity, gasRecord) + " x=" + x + " y=" + y);
                minx = Math.min(minx, x);
                maxx = Math.max(maxx, x);
                miny = Math.min(miny, y);
                maxy = Math.max(maxy, y);
                xNumbers.add(x);
                yNumbers.add(y);
                sumy += y;
            }
        }

        // adjust min/max values if no data
        if (xNumbers.isEmpty()) minx = maxx = 0;
        if (yNumbers.isEmpty()) miny = maxy = 0;

        // calculate average for the series
        average = 0;
        if (!yNumbers.isEmpty()) {
            average = sumy / yNumbers.size();
        }

        Timber.d(tag, "minx=" + minx + " maxx=" + maxx);
        Timber.d(tag, "miny=" + miny + " maxy=" + maxy);
        Timber.d(tag, "sumy=" + sumy + " size=" + yNumbers.size() + " average=" + average);

        // create a new series from the x and y axis numbers
        String title = "";
        return new SimpleXYSeries(xNumbers, yNumbers, title);
    }

    @Override
    protected void setPlotRangeLabel() {
        plot.setRangeLabel(units.getMileageLabel());
    }
}
