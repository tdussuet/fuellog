/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.plot;

import android.app.Activity;

import com.androidplot.xy.XYPlot;

import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.preferences.Units;
import ch.tdussuet.fuellog.util.OldMonthlyTrips;

/**
 * DESCRIPTION:
 * A plot of unit based data.
 */
abstract class UnitBasedPlot extends Plot {
    protected Units units;

    protected void setUnits(Activity activity) {
        units = new Units(SettingsActivity.KEY_UNITS, activity);
    }

    @Override
    public void createPlot(Activity parent, XYPlot xyplot, OldMonthlyTrips monthly) {
        setUnits(parent);
        super.createPlot(parent, xyplot, monthly);
    }

    @Override
    protected void handleSettingsChanged(String key) {
        super.handleSettingsChanged(key);
        handleUnitsChanged(key);
    }

    protected void setPlotRangeLabel() {
    }

    protected void handleUnitsChanged(String key) {
        if (key.equals(SettingsActivity.KEY_UNITS)) {
            setUnits(activity);
            setPlotRangeLabel();
            redrawPlot();
        }
    }
}

