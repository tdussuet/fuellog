/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.plot;

import androidx.annotation.NonNull;

import java.text.DecimalFormat;
import java.text.Format;

import ch.tdussuet.fuellog.item.TripRecord;

/**
 * DESCRIPTION:
 * A plot of odometer data (distance driven).
 */
public class OdometerPlot extends UnitBasedPlot {

    @Override
    protected Format getYlabels() {
        return new DecimalFormat("#######0");
    }

    @NonNull
    @Override
    protected String getPlotYLabel() {
        return units.getDistanceLabel();
    }

    @Override
    protected void setLinesPerLabel() {
        super.setLinesPerLabel();
        plot.setLinesPerDomainLabel(1);
    }

    @Override
    protected long getStartingYBoundary() {
        return 100;
    }

    @Override
    protected double getYValue(TripRecord monthTripRecord) {
        return monthTripRecord.getDistance();
    }

    @Override
    protected void setPlotRangeLabel() {
        plot.setRangeLabel(units.getDistanceLabel());
    }
}

