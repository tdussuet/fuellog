/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.plot;

import androidx.annotation.NonNull;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

import ch.tdussuet.fuellog.App;
import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.item.TripRecord;

/**
 * DESCRIPTION:
 * A plot of gas price data.
 */
public class PricePlot extends UnitBasedPlot {

    /// total gas used for plot period
    private TripRecord summary;

    @NonNull
    @Override
    protected String getPlotYLabel() {
        String format = activity.getString(R.string.title_plot_price_range);
        Locale locale = App.getLocale(activity);
        return String.format(locale, format, units.getLiquidVolumeRatioLabel());
    }

    @Override
    protected void setLinesPerLabel() {
        super.setLinesPerLabel();
        plot.setLinesPerDomainLabel(1);
    }

    @Override
    protected long getStartingYBoundary() {
        return 1;
    }

    @Override
    protected void increaseSumY(TripRecord monthTripRecord) {
        summary.append(monthTripRecord);
    }

    @Override
    protected double getYValue(TripRecord monthTripRecord) {
        return monthTripRecord.getPrice();
    }

    @Override
    protected void setSumY() {
        summary = new TripRecord(LocalDateTime.now());
    }

    @Override
    protected double calculateAverage(List<Number> yNumbers) {
        return summary.getPrice();
    }

    @Override
    protected void setPlotRangeLabel() {
        plot.setRangeLabel(units.getLiquidVolumeLabel());
    }
}

