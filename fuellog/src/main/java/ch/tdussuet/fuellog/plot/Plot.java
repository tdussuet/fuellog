/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.plot;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYRegionFormatter;
import com.androidplot.xy.XYSeriesFormatter;

import java.text.Format;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.item.TripRecord;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.ui.MappedLabelFormat;
import ch.tdussuet.fuellog.util.FontSizeUtil;
import ch.tdussuet.fuellog.util.OldMonthlyTrips;
import ch.tdussuet.fuellog.util.PlotUtil;
import ch.tdussuet.fuellog.util.format.CurrencyManager;
import lombok.Setter;
import lombok.val;
import timber.log.Timber;

/**
 * DESCRIPTION:
 * A plot of data.
 */
class Plot implements OnSharedPreferenceChangeListener {

    /// for logging
    private static final String TAG = Plot.class.getName();

    /// the parent activity
    protected Activity activity;

    /// the plot
    protected XYPlot plot;

    /// defines how the plot bars are drawn
    protected XYSeriesFormatter<XYRegionFormatter> plotFormatter;

    /// defines how the average line is drawn
    protected LineAndPointFormatter avgFormatter;

    /// defines how the average point label is drawn
    protected PointLabelFormatter avgLabelFormatter;

    /// average gas used per month for plot period
    protected double average = 0;

    /// range of y-axis data for the plot period (gas used)
    protected double miny = 0;
    protected double maxy = 0;

    /// range of x-axis data for the plot period (sequential index [0..n] mapped to months) 
    /// range of x-axis data for the plot period (time in milliseconds since 1970)
    protected long minx = 0;
    protected long maxx = 0;

    /// x-axis boundaries of the plot (based on minx/maxx)
    protected long lowerboundx = 0;
    protected long upperboundx = 0;

    /// formatter for x-axis labels - maps from x-axis values to month labels
    protected Format xlabels;

    /// formatter for y-axis labels
    protected Format ylabels;

    @Setter
    protected OldMonthlyTrips monthly;

    /**
     * DESCRIPTION:
     * Creates the graph.
     */
    public void createPlot(Activity parent, XYPlot xyplot, OldMonthlyTrips monthly) {
        this.activity = parent;
        this.plot = xyplot;
        this.monthly = monthly;
        this.xlabels = getXlabels();
        this.ylabels = getYlabels();
        setupPlot();
    }

    protected void setupPlot() {
        // create a formatter to use for drawing the plot series
        plotFormatter = getPlotFormatter();

        // create a formatter for average label
        float hOffset = 100f;
        float vOffset = -10f;
        avgLabelFormatter = new PointLabelFormatter(
                activity.getResources().getColor(R.color.plot_avgline_color, activity.getTheme()),
                hOffset,
                vOffset);

        // create a formatter to use for drawing the average line
        avgFormatter = new LineAndPointFormatter(
                activity.getResources().getColor(R.color.plot_avgline_color, activity.getTheme()),
                null,
                null,
                avgLabelFormatter);

        String rangeLabel = getPlotYLabel();
        String domainLabel = getPlotXLabel();
        PlotUtil.setPlotWidths(activity, plot, xlabels, ylabels, rangeLabel, domainLabel);

        setupSpecificPlot();
        drawPlot();
    }

    protected String getPlotXLabel() {
        return activity.getString(R.string.months_label);
    }

    protected void setupSpecificPlot() {
    }

    @NonNull
    protected XYSeriesFormatter<XYRegionFormatter> getPlotFormatter() {
        return new BarFormatter(
                activity.getResources().getColor(R.color.plot_fill_color, activity.getTheme()),
                activity.getResources().getColor(R.color.plot_line_color, activity.getTheme()));
    }

    protected Format getXlabels() {
        return new MappedLabelFormat();
    }

    protected Format getYlabels() {
        return CurrencyManager.getInstance(activity).getNumericFormatter();
    }

    @NonNull
    protected String getPlotYLabel() {
        return "";
    }

    /**
     * DESCRIPTION:
     * Performs the steps required to display the data in the plot widget.
     */
    protected void drawPlot() {
        FontSizeUtil.setPlotFontSizes(activity, plot, avgLabelFormatter);
        plot.addSeries(getPlotSeries(), plotFormatter);
        setPlotAxisBoundaries();
        addAverageLineToPlot();
    }

    protected void addAverageLineToPlot() {
        if (average > 0) {
            plot.addSeries(getAverageSeries(), avgFormatter);
            avgFormatter.setPointLabeler((series, index) ->
                    (index == 0) ? ylabels.format(series.getY(index)) : "");
        }
    }

    /**
     * DESCRIPTION:
     * Clears the plot widget, then plots the data again.
     */
    protected void redrawPlot() {
        plot.clear();
        drawPlot();
        plot.redraw();
    }

    protected void setPlotAxisBoundaries() {
        setupYAxis();
        setupXAxis();
        setLinesPerLabel();
        setupBarPlotSpecificSettings();
    }

    protected void setupBarPlotSpecificSettings() {
        long numberOfDataPoints = getNumberOfMonthsPlotted();
        PlotUtil.setBarWidth(activity, plot, numberOfDataPoints);
        abbreviateXLabelsIfNeeded(numberOfDataPoints);
    }

    protected void abbreviateXLabelsIfNeeded(long numberOfDataPoints) {
        boolean shouldAbbreviateLabels = numberOfDataPoints > 6;
        ((MappedLabelFormat) xlabels).setAbbreviate(shouldAbbreviateLabels);
    }

    protected long getNumberOfMonthsPlotted() {
        return maxx - minx + 1;
    }

    protected void setupXAxis() {
        lowerboundx = getLowerXBoundary();
        upperboundx = getUpperXBoundary();
        adjustXBoundariesIfNecessary();
        plot.setDomainBoundaries(lowerboundx, upperboundx, BoundaryMode.FIXED);
        val xStep = 1;
        plot.setDomainStep(StepMode.INCREMENT_BY_VAL, xStep);
        Timber.d(TAG, String.valueOf(lowerboundx));
        Timber.d(TAG, String.valueOf(upperboundx));
        Timber.d(TAG, String.valueOf(xStep));
    }

    protected void adjustXBoundariesIfNecessary() {
    }

    protected long getUpperXBoundary() {
        return maxx + 1;
    }

    protected long getLowerXBoundary() {
        return minx - 1;
    }

    protected void setupYAxis() {
        val lowerYBoundary = getLowerYBoundary();
        val upperYBoundary = getUpperYBoundary();
        plot.setRangeBoundaries(lowerYBoundary, upperYBoundary, BoundaryMode.FIXED);
        val yRange = upperYBoundary - lowerYBoundary;
        val yStep = getYStep(yRange);
        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, yStep);
        Timber.d(TAG, String.valueOf(lowerYBoundary));
        Timber.d(TAG, String.valueOf(upperYBoundary));
        Timber.d(TAG, String.valueOf(yStep));
    }

    protected double getYStep(double yRange) {
        return yRange / 10;
    }

    protected double getLowerYBoundary() {
        return 0;
    }

    protected double getUpperYBoundary() {
        long yBoundary = getStartingYBoundary();
        while (yBoundary <= maxy) yBoundary *= 2;
        return yBoundary;
    }

    protected void setLinesPerLabel() {
        plot.setLinesPerRangeLabel(2);
    }

    protected long getStartingYBoundary() {
        return 25;
    }

    /**
     * DESCRIPTION:
     * Obtains (x,y) values from the current data set for plotting. Also
     * calculates the range (min/max) of x-axis and y-axis values for the series.
     *
     * @return a SimpleXYSeries instance containing (x,y) values to plot.
     */
    protected SimpleXYSeries getPlotSeries() {
        final String tag = TAG + ".getPlotSeries()";

        // create lists of x-axis, and y-axis numbers to plot
        List<Number> xNumbers = new ArrayList<>();
        List<Number> yNumbers = new ArrayList<>();

        // get numbers to plot from gas record monthly data, where (x,y) is:
        // x = sequential index [0..n] with labels mapped to specific months
        // y = calculated Y for that month
        setSumY();
        miny = Float.MAX_VALUE;
        maxy = Float.MIN_VALUE;
        minx = Long.MAX_VALUE;
        maxx = Long.MIN_VALUE;
        clearXlabels();
        long x = 0L;
        double y;
        for (Month month : monthly) {
            TripRecord monthTripRecord = monthly.getTrips(month);
            y = getYValue(monthTripRecord);
            Timber.d(tag, "month=" + month.toString() + " x=" + x + " y=" + y);
            minx = Math.min(minx, x);
            maxx = Math.max(maxx, x);
            miny = Math.min(miny, y);
            maxy = Math.max(maxy, y);
            xNumbers.add(x);
            yNumbers.add(y);
            increaseSumY(monthTripRecord);
            addXLabel(x, month);
            x += 1;
        }

        // adjust min/max values if no data
        if (xNumbers.isEmpty()) minx = maxx = 0;
        if (yNumbers.isEmpty()) miny = maxy = 0;

        // calculate average for the series
        average = calculateAverage(yNumbers);

        Timber.d(tag, "minx=%s maxx=%s", minx, maxx);
        Timber.d(tag, "miny=%s maxy=%s", miny, maxy);
        Timber.d(tag, "average=%s", average);

        // create a new series from the x and y axis numbers
        String title = "";
        return new SimpleXYSeries(xNumbers, yNumbers, title);
    }

    private void addXLabel(long x, Month month) {
        ((MappedLabelFormat) xlabels).put(x, month.getDisplayName(TextStyle.SHORT, activity.getResources().getConfiguration().getLocales().get(0)));
    }

    private void clearXlabels() {
        ((MappedLabelFormat) xlabels).clear();
    }

    protected void increaseSumY(TripRecord monthTripRecord) {
    }

    protected double getYValue(TripRecord monthTripRecord) {
        return 0;
    }

    protected void setSumY() {
    }

    protected double calculateAverage(List<Number> yNumbers) {
        return yNumbers.stream()
                .mapToDouble(Number::doubleValue)
                .average()
                .orElse(0);
    }

    /**
     * DESCRIPTION:
     * Obtains (x,y) values for a line reflecting the average value
     * for the current data set.
     *
     * @return a SimpleXYSeries instance containing (x,y) values to plot.
     */
    protected SimpleXYSeries getAverageSeries() {
        // create lists of x-axis, and y-axis numbers to plot
        List<Number> xNumbers = new LinkedList<>();
        List<Number> yNumbers = new LinkedList<>();

        // line at average, across x-axis
        xNumbers.add(lowerboundx);
        yNumbers.add(average);
        xNumbers.add(upperboundx);
        yNumbers.add(average);

        // create a new series from the x and y axis numbers
        String title = "";
        return new SimpleXYSeries(xNumbers, yNumbers, title);
    }

    /**
     * DESCRIPTION:
     * Called when one or more plot preferences have changed.
     *
     * @see android.content.SharedPreferences.OnSharedPreferenceChangeListener#onSharedPreferenceChanged(android.content.SharedPreferences, java.lang.String)
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsActivity.KEY_PLOT_DATE_RANGE)) {
            // plot date range changed
            redrawPlot();
        }
        if (key.equals(SettingsActivity.KEY_PLOT_FONT_SIZE)) {
            // plot font size changed
            redrawPlot();
        }
        handleSettingsChanged(key);
    }

    protected void handleSettingsChanged(String key) {
    }

    /**
     * DESCRIPTION:
     * Sets the height of the plot view.
     *
     * @param height - the height in pixels.
     */
    public void setHeight(int height) {
        ViewGroup.LayoutParams params = plot.getLayoutParams();
        params.height = height;
        plot.setLayoutParams(params);
        plot.redraw();
    }
}

