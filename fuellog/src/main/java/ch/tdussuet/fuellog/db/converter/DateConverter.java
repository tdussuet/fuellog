package ch.tdussuet.fuellog.db.converter;

import androidx.room.TypeConverter;

import java.time.LocalDate;

public class DateConverter {

    @TypeConverter
    public static LocalDate toDate(Long dateLong) {
        return dateLong == null ? null : LocalDate.ofEpochDay(dateLong);
    }

    @TypeConverter
    public static Long fromDate(LocalDate date) {
        return date == null ? null : date.toEpochDay();
    }
}
