package ch.tdussuet.fuellog.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(tableName = "records")
public class GasRecord implements Serializable {
    @PrimaryKey
    @ColumnInfo(name = "_id")
    public int id;

    @ColumnInfo(name = "_vid")
    public int vehicleID;

    @ColumnInfo(name = "time")
    public LocalDateTime date;

    @ColumnInfo(name = "gallons")
    public float amount;

    @ColumnInfo
    public int odometer;

    @ColumnInfo
    public double cost;

    @ColumnInfo(defaultValue = "0")
    public double price;

    @ColumnInfo
    public String notes;

    @ColumnInfo
    public boolean fullTank;

    @ColumnInfo
    public boolean hidden;

    @ColumnInfo(defaultValue = "0")
    public float mileage;
}
