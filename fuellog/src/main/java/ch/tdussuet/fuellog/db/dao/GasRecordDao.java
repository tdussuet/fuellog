package ch.tdussuet.fuellog.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.time.LocalDate;
import java.util.List;

import ch.tdussuet.fuellog.db.entity.GasRecord;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;

@Dao
public interface GasRecordDao {
    @Query("SELECT * FROM records")
    Flowable<List<GasRecord>> getAll();

    @Query("SELECT * FROM records WHERE _vid = :vehicleId")
    Single<List<GasRecord>> findAllByVehicle(int vehicleId);

    @Query("SELECT * FROM records WHERE _vid = :vehicleId AND time BETWEEN :startDate AND :endDate")
    Single<List<GasRecord>> findByDate(int vehicleId, LocalDate startDate, LocalDate endDate);

    @Query("SELECT MAX(odometer) FROM RECORDS WHERE _vid = :vehicleId")
    Single<Integer> getCurrentOdometer(int vehicleId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(GasRecord gasRecord);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertAll(List<GasRecord> gasRecord);

    @Update
    Completable update(GasRecord gasRecord);

    @Delete
    Completable delete(GasRecord gasRecord);
}