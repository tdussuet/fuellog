package ch.tdussuet.fuellog.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ch.tdussuet.fuellog.db.entity.Vehicle;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

@Dao
public interface VehicleDao {
    @Query("SELECT * FROM vehicles")
    Single<List<Vehicle>> getAll();

    @Query("SELECT * FROM vehicles WHERE _id IN (:vehicleIds)")
    Single<List<Vehicle>> loadAllByIds(int[] vehicleIds);

    @Query("SELECT * FROM vehicles WHERE name LIKE :name LIMIT 1")
    Single<Vehicle> findByName(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(Vehicle vehicle);

    @Update
    Completable update(Vehicle vehicle);

    @Delete
    Completable delete(Vehicle vehicle);
}