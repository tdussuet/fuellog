package ch.tdussuet.fuellog.db;

import android.content.Context;

import androidx.room.AutoMigration;
import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import java.io.File;

import ch.tdussuet.fuellog.db.converter.DateConverter;
import ch.tdussuet.fuellog.db.converter.DateTimeConverter;
import ch.tdussuet.fuellog.db.dao.GasRecordDao;
import ch.tdussuet.fuellog.db.dao.VehicleDao;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.db.entity.Vehicle;

@Database(entities = {Vehicle.class, GasRecord.class}, version = FuelDatabase.DB_VERSION, exportSchema = true, autoMigrations = {
        @AutoMigration(from = 5, to = 6)
})
@TypeConverters({DateConverter.class, DateTimeConverter.class})
public abstract class FuelDatabase extends RoomDatabase {
    public static final int DB_VERSION = 6;
    public static final String DB_NAME = "gaslog.db";

    /**
     * DESCRIPTION:
     * Determines if the log database file currently exists.
     *
     * @return true if file is missing, false otherwise.
     */
    public static boolean isDatabaseFileMissing(Context context) {
        File dbFile = context.getDatabasePath(DB_NAME);
        return !dbFile.exists();
    }

    public abstract VehicleDao vehicleDao();
    public abstract GasRecordDao gasRecordDao();
}
