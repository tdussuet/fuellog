package ch.tdussuet.fuellog.db.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import lombok.Data;

@Data
@Entity(tableName = "vehicles")
public class Vehicle implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    @NonNull
    public Integer id;

    @ColumnInfo
    @NonNull
    public String name;

    @ColumnInfo
    public float tankSize;
}

