/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.util;

import android.content.Context;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import ch.tdussuet.fuellog.preferences.Units;

/**
 * DESCRIPTION:
 * A collection of static methods that perform commonly used tasks on
 * a java list of gas records (List<GasRecord>).
 * <p>
 */
public class GasRecordUtil {

    protected static final String TAG = GasRecordUtil.class.getName();

    /**
     * DESCRIPTION:
     * Calculates gas mileage for a List of GasRecords.
     * NOTE: The resulting list is sorted by odometer value.
     *
     * @param list    - the GasRecord List.
     * @param context - the application context.
     */
    public static void calculateMileage(List<GasRecord> list, Context context) {

        //TODO get rid of this

        // do nothing if list is empty
        if ((list == null) || list.isEmpty())
            return;

        // sort the list by odometer value
        list.sort(new OdometerComparator());

        // get currently selected units of measurement for calculation
        Units units = new Units(SettingsActivity.KEY_UNITS, context);

        // initialize for calculations
        GasRecord gasRecord;
        MileageCalculation calc = null;
        Iterator<GasRecord> iterator = list.iterator();

        // find the first full tank
        while (iterator.hasNext()) {
            gasRecord = iterator.next();
            gasRecord.setMileage(0);
            if (gasRecord.isFullTank()) {
                calc = new MileageCalculation(gasRecord, units, context);
                break;
            }
        }

        if (calc == null) {
            return;
        }

        // start calculating mileage after first full tank
        while (iterator.hasNext()) {
            gasRecord = iterator.next();
            calc.add(gasRecord);
            if (gasRecord.isFullTank()) {
                gasRecord.setMileage(calc.getMileage());
                calc = new MileageCalculation(gasRecord, units, context);
            } else {
                gasRecord.setMileage(0);
            }
        }
    }

    /**
     * DESCRIPTION:
     * Locates a record in the list.
     * NOTE: Odometer value is used for comparison.
     *
     * @param list   - the list of gas records.
     * @param gasRecord - the record to search for.
     * @return the index of the record in the list (negative if not found).
     */
    public static int find(List<GasRecord> list, GasRecord gasRecord) {
        return Collections.binarySearch(list, gasRecord, new OdometerComparator());
    }

    /**
     * DESCRIPTION:
     * Searches a list to determine if it contains a record with a full tank.
     *
     * @param list - the list of gas records.
     * @return true if list contains a record with full tank.
     */
    public static boolean hasFullTank(List<GasRecord> list) {
        return list.stream().anyMatch(GasRecord::isFullTank);
    }

    /**
     * DESCRIPTION:
     * Searches for a previous record in the list with a full tank.
     *
     * @param list     - the list of gas records.
     * @param location - the location in the list to start searching.
     * @return the index of the previous full tank in the list (negative if not found)/
     */
    public static int findPreviousFullTank(List<GasRecord> list, int location) {
        if (list == null || location >= list.size()) {
            return -1;
        }
        int previous = location - 1;
        while (previous >= 0) {
            if (list.get(previous).isFullTank())
                return previous;
            previous--;
        }
        return -1;
    }
}
