/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2014 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.util.format;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;

import java.util.Arrays;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import ch.tdussuet.fuellog.App;
import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.preferences.SettingsActivity;
import timber.log.Timber;

/**
 * DESCRIPTION:
 * Manages currency settings and the formatting of currency values for
 * specific locales.
 */
public class CurrencyManager implements OnSharedPreferenceChangeListener {

    /// for logging
    private static final String TAG = CurrencyManager.class.getName();

    /// key string for default currency (application locale currency)
    public final String DEFAULT_CURRENCY;

    /// singleton instance
    private static CurrencyManager instance = null;

    /// map of currency key strings to currency locales
    private final Map<String, Locale> localeMap = new HashMap<>();

    /// locale for currently selected currency
    private Locale locale;

    /// a formatter for currency values as strings including appropriate currency symbols
    private CurrencyFormatter symbolicFormatter = null;

    /// a formatter for currency values as strings NOT including appropriate currency symbols
    private CurrencyFormatter numericFormatter = null;

    /// a formatter for fractional currency values as strings including appropriate currency symbols
    private CurrencyFormatter symbolicFractionalFormatter = null;

    /// a formatter for fractional currency values as strings NOT including appropriate currency symbols
    private CurrencyFormatter numericFractionalFormatter = null;

    private final Context context;

    /**
     * DESCRIPTION:
     * Obtains a singleton instance of the CurrencyManager
     *
     * @return CurrencyManager instance.
     */
    public static CurrencyManager getInstance(Context context) {
        if (instance == null) {
            instance = new CurrencyManager(context);
        }
        return instance;
    }

    /**
     * DESCRIPTION:
     * Returns a key string for a specific currency/locale.
     *
     * @param locale   locale for the lookup
     * @param currency currency for the lookup
     * @return String corresponding to the key
     */
    private static String getKey(Locale locale, Currency currency, Context context) {
        return String.format("%s - %s",
                currency.getCurrencyCode(),
                locale.getDisplayName(App.getLocale(context)));
    }

    /**
     * DESCRIPTION:
     * Initializes the currency setting to a default value if no other
     * value has been selected yet.
     */
    private void initialize() {
        if (SettingsActivity.getString(SettingsActivity.KEY_CURRENCY, null, context) == null) {
            SettingsActivity.setString(SettingsActivity.KEY_CURRENCY, DEFAULT_CURRENCY, context);
        }
    }

    /**
     * DESCRIPTION:
     * Obtains a list of available currencies and generates a map
     * to define key/value pairs.
     */
    private void getAvailableCurrencies() {

        final String tag = TAG + ".getAvailableCurrencies()";

        Locale[] locales = Locale.getAvailableLocales();

        for (Locale locale : locales) {
            try {
                Timber.d(tag, locale.getLanguage() + " " + locale.getCountry());
                Currency currency = Currency.getInstance(locale);
                String key = getKey(locale, currency, context);
                localeMap.put(key, locale);
            } catch (IllegalArgumentException ex) {
                Timber.d(tag, "locale's country is not a supported ISO 3166 country: %s", locale.getCountry());
            }
        }

        localeMap.put(DEFAULT_CURRENCY, App.getLocale(context));
    }

    /**
     * DESCRIPTION:
     * Constructs an instance of CurrencyManager.
     */
    private CurrencyManager(Context context) {
        this.context = context;
        initialize();
        getAvailableCurrencies();
        DEFAULT_CURRENCY = context.getString(R.string.default_currency);

        // get locale for preferred currency
        getCurrencyLocale();

        // setup to be notified when shared preferences change
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    /**
     * DESCRIPTION:
     * Get locale for preferred currency.
     */
    private void getCurrencyLocale() {
        String key = SettingsActivity.getString(SettingsActivity.KEY_CURRENCY, DEFAULT_CURRENCY, context);
        if (key == null) {
            key = DEFAULT_CURRENCY;
            SettingsActivity.setString(SettingsActivity.KEY_CURRENCY, key, context);
        }
        locale = localeMap.getOrDefault(key, App.getLocale(context));
    }

    /**
     * DESCRIPTION:
     * Returns the symbol for the currently selected currency/locale.
     *
     * @return currency symbol as a String
     */
    public String getCurrencySymbol() {
        String symbol = "?";
        try {
            symbol = Currency.getInstance(locale).getSymbol(locale);
        } catch (IllegalArgumentException e) {
            Timber.e(TAG + ".getCurrencySymbol()", "unable to get symbol", e);
        }
        return symbol;
    }

    /**
     * DESCRIPTION:
     * Returns a list of preference entry strings for selection as
     * a currency setting.
     *
     * @return array of entry Strings
     */
    public String[] getPrefEntries() {
        Set<String> keyset = localeMap.keySet();
        keyset.remove(null);
        String[] entries = keyset.toArray(new String[0]);
        Arrays.sort(entries);
        return entries;
    }

    /**
     * DESCRIPTION:
     * Returns a list of preference entry value strings for selection as
     * a currency setting.
     *
     * @return array of entry value Strings
     */
    public String[] getPrefEntryValues() {
        return getPrefEntries();
    }

    /**
     * DESCRIPTION:
     * Returns a summary string to describe the current currency setting.
     *
     * @return summary String
     */
    public String getPrefSummary() {
        return SettingsActivity.getString(SettingsActivity.KEY_CURRENCY, DEFAULT_CURRENCY, context);
    }

    /**
     * DESCRIPTION:
     * Obtains an instance of a currency formatter for display
     * of currency values with currency symbol.
     *
     * @return CurrencyFormatter
     */
    public CurrencyFormatter getSymbolicFormatter() {

        if (symbolicFormatter == null) {
            symbolicFormatter = new CurrencyFormatter(false);
            symbolicFormatter.setLocale(locale);
        }

        return symbolicFormatter;
    }

    /**
     * DESCRIPTION:
     * Obtains an instance of a currency formatter for display
     * of currency values without currency symbol.
     *
     * @return CurrencyFormatter
     */
    public CurrencyFormatter getNumericFormatter() {

        if (numericFormatter == null) {
            numericFormatter = new CurrencyFormatter(true);
            numericFormatter.setLocale(locale);
        }

        return numericFormatter;
    }

    /**
     * DESCRIPTION:
     * Obtains an instance of a currency formatter for display
     * of fractional currency values with currency symbol.
     *
     * @return CurrencyFormatter
     */
    public CurrencyFormatter getSymbolicFractionalFormatter() {

        if (symbolicFractionalFormatter == null) {
            symbolicFractionalFormatter = new FractionalCurrencyFormatter(false);
            symbolicFractionalFormatter.setLocale(locale);
        }

        return symbolicFractionalFormatter;
    }

    /**
     * DESCRIPTION:
     * Obtains an instance of a currency formatter for display
     * of fractional currency values without currency symbol.
     *
     * @return PriceFormatter
     */
    public CurrencyFormatter getNumericFractionalFormatter() {

        if (numericFractionalFormatter == null) {
            numericFractionalFormatter = new FractionalCurrencyFormatter(true);
            numericFractionalFormatter.setLocale(locale);
        }

        return numericFractionalFormatter;
    }

    /**
     * DESCRIPTION:
     * Called when Settings have changed. Updates the formatters to utilize the
     * selected currency/locale.
     *
     * @see android.content.SharedPreferences.OnSharedPreferenceChangeListener#onSharedPreferenceChanged(android.content.SharedPreferences, java.lang.String)
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsActivity.KEY_CURRENCY)) {
            getCurrencyLocale();
            if (symbolicFormatter != null) symbolicFormatter.setLocale(locale);
            if (numericFormatter != null) numericFormatter.setLocale(locale);
            if (symbolicFractionalFormatter != null) symbolicFractionalFormatter.setLocale(locale);
            if (numericFractionalFormatter != null) numericFractionalFormatter.setLocale(locale);
        }

    }

}