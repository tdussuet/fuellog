/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */
package ch.tdussuet.fuellog.util;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Insets;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.WindowMetrics;

import androidx.annotation.NonNull;

import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BarRenderer;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;

import java.text.Format;

public class PlotUtil {
    public static void setPlotWidths(Activity activity,
                                     XYPlot plot,
                                     Format xlabels,
                                     Format ylabels,
                                     String rangeLabel,
                                     String domainLabel) {
        // white background for the plot
        XYGraphWidget graphWidget = plot.getGraph();
        graphWidget.getGridBackgroundPaint().setColor(Color.WHITE);

        // remove the series legend
        plot.getLayoutManager().remove(plot.getLegend());

        // make room for bigger labels
        // TODO: is there a better way to do this?
        float width = graphWidget.getRangeCursorPaint().getStrokeWidth() * 2f;
        graphWidget.getRangeCursorPaint().setStrokeWidth(width);
        width = graphWidget.getDomainCursorPaint().getStrokeWidth() * 1.5f;
        graphWidget.getRangeCursorPaint().setStrokeWidth(width);
        float margin = graphWidget.getMarginTop() * 3f;
        graphWidget.setMarginTop(margin);
        margin = graphWidget.getMarginBottom() * 3f;
        graphWidget.setMarginBottom(margin);

        // define plot axis labels
        plot.setRangeLabel(rangeLabel);
        plot.setDomainLabel(domainLabel);

        // specify format of axis value labels
        graphWidget.setLineLabelEdges(XYGraphWidget.Edge.BOTTOM, XYGraphWidget.Edge.LEFT);
        graphWidget.getLineLabelStyle(XYGraphWidget.Edge.LEFT).setFormat(ylabels);
        graphWidget.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(xlabels);
        graphWidget.setPadding(PixelUtils.dpToPix(60), PixelUtils.dpToPix(5), PixelUtils.dpToPix(0), PixelUtils.dpToPix(30));

        graphWidget.getLineLabelInsets().setLeft(PixelUtils.dpToPix(-25));
        graphWidget.getLineLabelInsets().setBottom(PixelUtils.dpToPix(-15));
    }

    public static void setBarWidth(Activity activity, XYPlot plot, long months) {
        // adjust bar thickness based on number of months being plotted
        BarRenderer<?> barRenderer = (BarRenderer<?>) plot.getRenderer(BarRenderer.class);
        if (barRenderer != null) {
            WindowManager windowManager = activity.getWindowManager();
            int width;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                Rect bounds = windowManager.getCurrentWindowMetrics().getBounds();
                width = bounds.width();
            } else {
                width = getScreenWidth(activity);
            }
            float displayWidth = Utilities.convertPixelsToDp(width, activity);
            float plotWidth = displayWidth * 0.75f;
            float barWidth = plotWidth / months;
            barRenderer.setBarGroupWidth(BarRenderer.BarGroupWidthMode.FIXED_WIDTH, barWidth);
        }
    }

    private static int getScreenWidth(@NonNull Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            WindowMetrics windowMetrics = activity.getWindowManager().getCurrentWindowMetrics();
            Insets insets = windowMetrics.getWindowInsets()
                    .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars());
            return windowMetrics.getBounds().width() - insets.left - insets.right;
        } else {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            return displayMetrics.widthPixels;
        }
    }
}
