/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */
package ch.tdussuet.fuellog.util;

import android.app.Activity;

import com.androidplot.ui.widget.TextLabelWidget;
import com.androidplot.util.PixelUtils;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;

import ch.tdussuet.fuellog.preferences.SettingsActivity;

public class FontSizeUtil {

    /**
     * DESCRIPTION:
     * Adjust font sizes used for plot labels to reflect shared
     * preferences.
     */
    public static void setPlotFontSizes(Activity activity, XYPlot plot, PointLabelFormatter avgLabelFormatter) {
        PlotFontSize size = new PlotFontSize(activity, SettingsActivity.KEY_PLOT_FONT_SIZE);

        // plot title label
        float textSize = PixelUtils.dpToPix(size.getSizeDp());
        TextLabelWidget titleWidget = plot.getTitle();
        titleWidget.getLabelPaint().setTextSize(textSize);
        titleWidget.pack();

        // axis step value labels
        XYGraphWidget graphWidget = plot.getGraph();
        graphWidget.getRangeCursorPaint().setTextSize(textSize);
        graphWidget.getDomainCursorPaint().setTextSize(textSize);

        // axis origin value labels
        graphWidget.getRangeOriginLinePaint().setTextSize(textSize);
        graphWidget.getDomainOriginLinePaint().setTextSize(textSize);

        // axis title labels
        plot.getRangeTitle().getLabelPaint().setTextSize(textSize);
        plot.getDomainTitle().getLabelPaint().setTextSize(textSize);
        plot.getRangeTitle().pack();
        plot.getDomainTitle().pack();

        // average point label
        avgLabelFormatter.getTextPaint().setTextSize(textSize);
    }
}

