/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import java.time.LocalDateTime;

import ch.tdussuet.fuellog.R;

/**
 * DESCRIPTION:
 * Represents a preference value that specifies a range of dates to plot.
 * Provides the ability to obtain the current preference value and test whether a
 * specified Java Date instance falls within the range of dates.
 */
public class PlotDateRange {

    /// preference values represented as integers
    public static final int PAST_MONTH = 0;
    public static final int PAST_6_MONTHS = 1;
    public static final int PAST_12_MONTHS = 2;
    public static final int YEAR_TO_DATE = 3;
    public static final int ALL = 4;

    /// context for obtaining resources, etc
    private final Context context;

    /// the currently selected preference value
    private final int dateRange;

    /// start and end dates for the range
    private final LocalDateTime startDate;
    private final LocalDateTime endDate;


    /**
     * DESCRIPTION:
     * Constructs an instance of PlotDateRange.
     *
     * @param context - the context of the preferences whose values are wanted.
     * @param key     - the name of the preference to retrieve.
     */
    public PlotDateRange(Context context, String key) {

        // save context for future use
        this.context = context;

        // get saved preference value
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String dateRangePreference = prefs.getString(key, "0");

        // convert string to integer
        this.dateRange = Integer.parseInt(dateRangePreference);

        // determine start dates for specified range
        var firstStartDate = LocalDateTime.now()
                .withDayOfMonth(1)
                .withHour(0)
                .withMinute(0)
                .withSecond(0);
        switch (this.dateRange) {
            case ALL:
                // force maximum range to 2 years or plot gets ugly (too much data)
                firstStartDate = firstStartDate.minusMonths(23);
                break;
            case PAST_MONTH:
                break;
            case PAST_6_MONTHS:
                firstStartDate = firstStartDate.minusMonths(5);
                break;
            case PAST_12_MONTHS:
                firstStartDate = firstStartDate.minusMonths(11);
                break;
            case YEAR_TO_DATE:
                firstStartDate = firstStartDate.withMonth(1)
                        .withDayOfMonth(1);
                break;
            default:
                throw new RuntimeException("Invalid PlotDateRange integer value");
        }

        startDate = firstStartDate;

        // end date is midnight the first day of the next month
        endDate = LocalDateTime.now()
                .plusMonths(1)
                .withDayOfMonth(1)
                .withHour(0)
                .withMinute(0)
                .withSecond(0);
    }

    /**
     * DESCRIPTION:
     * Returns the preference value as an integer.
     *
     * @return the int value.
     * <p>
     * NOTE: The value is retrieved from shared preferences ONLY when
     * the instance is constructed.
     */
    public int getDateRange() {
        return this.dateRange;
    }

    /**
     * DESCRIPTION:
     * Returns a summary String describing the current preference value.
     * The strings are defined as resources.
     *
     * @return a summary String for the current preference value.
     */
    public String getSummary() {
        Resources resources = context.getResources();
        String[] entries = resources.getStringArray(R.array.arrayPlotDateRangeEntries);
        return entries[dateRange];
    }

    /**
     * DESCRIPTION:
     * Getter method for start of date range.
     *
     * @return the starting Date for the current range.
     */
    public LocalDateTime getStartDate() {
        return LocalDateTime.from(this.startDate);
    }

    /**
     * DESCRIPTION:
     * Getter method for end of date range.
     *
     * @return the ending Date for the current range.
     */
    public LocalDateTime getEndDate() {
        return LocalDateTime.from(this.endDate);
    }

    /**
     * DESCRIPTION:
     * Determines if a specified date falls within the current plot date range.
     *
     * @param date - the Date to test.
     * @return true if specified date is within range, false otherwise.
     */
    public boolean contains(LocalDateTime date) {
        return !(date.isBefore(startDate) || date.isAfter(endDate));
    }

}
