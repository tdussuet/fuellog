/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import ch.tdussuet.fuellog.R;

/**
 * DESCRIPTION:
 * An Activity to display and modify application settings/preferences.
 */
public class SettingsActivity extends AppCompatActivity {

    // shared preference keys
    public static final String KEY_UNITS = "units";
    public static final String KEY_PLOT_DATE_RANGE = "plot_date_range";
    public static final String KEY_PLOT_FONT_SIZE = "plot_font_size";
    public static final String KEY_DATA_ENTRY_MODE = "data_entry_mode";
    public static final String KEY_CURRENCY = "currency";

    /// tag string for logging
    private static final String TAG = SettingsActivity.class.getName();

    /**
     * DESCRIPTION:
     * Creates the Activity.
     *
     * @see androidx.appcompat.app.AppCompatActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();

    }

    /**
     * DESCRIPTION:
     * Returns flag to indicate whether entering a value for cost
     * is required when editing/creating a gas record.
     *
     * @return boolean - true if cost entry is required.
     */
    public static boolean isCostRequired(Context context) {
        String key = context.getString(R.string.pref_key_require_cost);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(key, true);
    }

    /**
     * DESCRIPTION:
     * Returns flag to indicate whether cost value should be
     * displayed in the gas log.
     *
     * @return boolean - true if cost value should be displayed.
     */
    public static boolean isCostDisplayable(Context context) {
        String key = context.getString(R.string.pref_key_display_cost);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(key, true);
    }

    /**
     * DESCRIPTION:
     * Returns flag to indicate whether notes value should be
     * displayed in the gas log.
     *
     * @return boolean - true if notes value should be displayed.
     */
    public static boolean isNotesDisplayable(Context context) {
        String key = context.getString(R.string.pref_key_display_notes);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(key, true);
    }

    /**
     * DESCRIPTION:
     * Retrieve a String value from the preferences.
     *
     * @param key          - the name of the preference to retrieve
     * @param defaultValue - vale to return if preference does not exist
     * @return String
     */
    public static String getString(String key, String defaultValue, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(key, defaultValue);
    }

    /**
     * DESCRIPTION:
     * Set a String value in the preferences.
     *
     * @param key   - the name of the preference to set
     * @param value - the new value for the preference
     */
    public static void setString(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

}
