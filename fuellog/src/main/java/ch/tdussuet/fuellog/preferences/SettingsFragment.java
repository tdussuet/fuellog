package ch.tdussuet.fuellog.preferences;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import java.text.DateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import ch.tdussuet.fuellog.App;
import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.activity.HtmlViewerActivity;
import ch.tdussuet.fuellog.db.FuelDatabase;
import ch.tdussuet.fuellog.util.PlotFontSize;
import ch.tdussuet.fuellog.util.format.CurrencyManager;
import timber.log.Timber;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = SettingsFragment.class.getName();

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);

        String key;
        Preference preference;
        final Activity activity = getActivity();
        assert activity != null;

        // populate list of available currencies
        CharSequence[] entries = CurrencyManager.getInstance(activity).getPrefEntries();
        CharSequence[] entryValues = CurrencyManager.getInstance(activity).getPrefEntryValues();
        key = getResources().getString(R.string.pref_key_currency);
        ListPreference lp = findPreference(key);
        lp.setEntries(entries);
        lp.setEntryValues(entryValues);

        // display the package name
        key = getResources().getString(R.string.pref_key_pkg_name);
        preference = findPreference(key);
        preference.setSummary(getActivity().getPackageName());

        // display the package version
        key = getResources().getString(R.string.pref_key_pkg_version);
        preference = findPreference(key);
        preference.setSummary(getPackageVersion());

        // display the database version
        key = getResources().getString(R.string.pref_key_database_version);
        preference = findPreference(key);
        preference.setSummary(getDatabaseVersion());

        // display the package build date
        key = getResources().getString(R.string.pref_key_build_date);
        preference = findPreference(key);
        preference.setSummary(getBuildDate());

        // display the license information when clicked (html)
        key = getResources().getString(R.string.pref_key_license);
        preference = findPreference(key);
        preference.setOnPreferenceClickListener(preference1 -> {
            Intent intent = new Intent(activity, HtmlViewerActivity.class);
            intent.putExtra(HtmlViewerActivity.URL, getString(R.string.url_license_html));
            activity.startActivity(intent);
            return true;
        });

        // display the help information when clicked (html)
        key = getResources().getString(R.string.pref_key_help);
        preference = findPreference(key);
        preference.setOnPreferenceClickListener(preference12 -> {
            Intent intent = new Intent(activity, HtmlViewerActivity.class);
            intent.putExtra(HtmlViewerActivity.URL, getString(R.string.url_help_html));
            activity.startActivity(intent);
            return true;
        });

        // register for notification of changes
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        // initialize summary for each shared preference to reflect the selected value
        onSharedPreferenceChanged(sharedPreferences, SettingsActivity.KEY_UNITS);
        onSharedPreferenceChanged(sharedPreferences, SettingsActivity.KEY_PLOT_FONT_SIZE);
        onSharedPreferenceChanged(sharedPreferences, SettingsActivity.KEY_CURRENCY);
    }

    /**
     * DESCRIPTION:
     * Returns the version for this application package.
     *
     * @return the package version String
     */
    @NonNull
    private String getPackageVersion() {
        try {
            return getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (Exception t) {
            Timber.tag(TAG).e(t, "Error obtaining package version");
            return "error";
        }
    }

    /**
     * DESCRIPTION:
     * Returns the build date for this application package.
     *
     * @return the package build date formatted as a String.
     */
    private String getBuildDate() {
        try {
            ApplicationInfo ai = getActivity().getPackageManager().getApplicationInfo(getActivity().getPackageName(), 0);
            ZipEntry ze;
            try (ZipFile zf = new ZipFile(ai.sourceDir)) {
                ze = zf.getEntry("classes.dex");
            }
            long time = ze.getTime();
            return DateFormat.getDateTimeInstance().format(new java.util.Date(time));
        } catch (Exception t) {
            Timber.tag(TAG).e(t, "Error obtaining build date/time");
            return "error";
        }
    }

    /**
     * DESCRIPTION:
     * Returns the database version for this application (actual and desired).
     *
     * @return the database version formatted as a String.
     */
    private String getDatabaseVersion() {
        String value;
        try {
            int actual = FuelDatabase.DB_VERSION;
            int desired = FuelDatabase.DB_VERSION;
            value = String.format(App.getLocale(getActivity()), "%d (%d)", actual, desired);
        } catch (Exception t) {
            Timber.tag(TAG).e(t, "Error obtaining database version");
            value = "error";
        }
        return value;
    }

    /**
     * DESCRIPTION:
     * Called when a preference value changes.
     *
     * @see SharedPreferences.OnSharedPreferenceChangeListener#onSharedPreferenceChanged(SharedPreferences, String)
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        final String tag = TAG + ".onSharedPrefChanged()";

        // get preference instance for the specified key
        Preference pref = findPreference(key);
        if (pref == null) {
            Timber.tag(tag).e("findPreference() returned null for key=%s", key);
            return;
        }

        // update the preference's summary to reflect the current value
        switch (key) {
            case SettingsActivity.KEY_UNITS:
                Units units = new Units(key, getActivity());
                pref.setSummary(units.getSummary());
                break;
            case SettingsActivity.KEY_PLOT_FONT_SIZE:
                PlotFontSize size = new PlotFontSize(getActivity(), key);
                pref.setSummary(size.getSummary());
                break;
            case SettingsActivity.KEY_CURRENCY:
                pref.setSummary(CurrencyManager.getInstance(getActivity()).getPrefSummary());
                break;
        }

    }
}
