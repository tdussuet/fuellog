/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.html;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.time.format.TextStyle;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.item.GasRecordCalculator;
import ch.tdussuet.fuellog.item.TripRecord;
import timber.log.Timber;

/**
 * DESCRIPTION:
 * A report for display of statistics derived from monthly trip data.
 */
public class StatisticsReport implements HtmlData {

    /// a tag string for debug logging (the name of this class)
    private static final String TAG = StatisticsReport.class.getName();

    /// end of line string
    private static final String NEWLINE = System.getProperty("line.separator");

    /// the report title
    private final String title;

    /// the monthly trip data for the report
    private final List<GasRecord> monthly;

    /// the html data for the report
    private StringBuilder html;

    /// one table for each month of statistical data
    private List<HtmlData> tables;

    private final Context context;

    /**
     * DESCRIPTION:
     * Constructs an instance of StatisticsReport.
     *
     * @param title   - the report title
     * @param monthly - the monthly trip data used to generate the report.
     */
    public StatisticsReport(String title, List<GasRecord> monthly, Context context) {

        this.title = title;
        this.monthly = monthly;
        this.context = context;

        // create the statistics tables
        createTables();

        // create the entire report from the table data
        createReport();
    }

    /**
     * DESCRIPTION:
     * Returns the report as an HTML String.
     *
     * @see HtmlData#getHtml()
     */
    @Override
    public String getHtml() {
        return html.toString();
    }

    /**
     * DESCRIPTION:
     * Generates statistical tables for the report.
     */
    private void createTables() {
        tables = new LinkedList<>();
        List<TripRecord> months = GasRecordCalculator.getMonthlySummary(monthly);

        // create tables for months in range
        for (TripRecord month : months) {
            tables.add(0, new StatisticsMonthTable(month, getMonthLabel(month), context));
        }

        // create table for summary of all data in range
        // note: no need for summary if only displaying one month table
        if (months.size() > 1) {
            tables.add(0, new StatisticsSummaryTable(months, title, context));
        }

    }

    private String getMonthLabel(TripRecord month) {
        var locale = context.getResources().getConfiguration().getLocales().get(0);
        var date = month.getEndDate();
        var monthLabel = date.getMonth().getDisplayName(TextStyle.SHORT, locale);
        var yearLabel = date.getYear();
        return monthLabel + " " + yearLabel;
    }

    /**
     * DESCRIPTION:
     * Generates an HTML page for the report.
     */
    private void createReport() {
        String stats_top = context.getString(R.string.asset_stats_top_html);
        String stats_bottom = context.getString(R.string.asset_stats_bottom_html);
        html = new StringBuilder();
        try {
            // append stats_top.html
            html.append(readAssetFile(stats_top));

            // append table data
            for (HtmlData table : tables) {
                html.append("<div>").append(NEWLINE);
                html.append(table.getHtml());
                html.append("</div>").append(NEWLINE);
                html.append("<p/>").append(NEWLINE);
            }

            // append stats_bottom.html
            html.append(readAssetFile(stats_bottom));
        } catch (Exception t) {
            String errorMessage = context.getString(R.string.toast_create_report_failed);
            Timber.tag(TAG).e(t, "Error creating report");
            html = new StringBuilder();
            html.append("<html>");
            html.append(errorMessage).append("<br/>");
            html.append(t.getMessage());
            html.append("/html>");
        }

    }

    /**
     * DESCRIPTION:
     * Reads the content of a specified asset file as String data.
     *
     * @param asset - the name of the asset file.
     * @return the content of the file as a String.
     * @throws IOException if an error occurs reading the file.
     */
    private String readAssetFile(String asset) throws IOException {
        AssetManager assetManager = context.getAssets();
        StringBuilder text = new StringBuilder();
        try (Scanner scanner = new Scanner(assetManager.open(asset))) {
            while (scanner.hasNextLine()) {
                text.append(scanner.nextLine());
                text.append(NEWLINE);
            }
        }

        return text.toString();
    }

}
