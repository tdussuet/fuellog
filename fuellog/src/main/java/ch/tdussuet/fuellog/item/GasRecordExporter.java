/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.item;

import android.content.Context;
import android.net.Uri;

import androidx.room.Room;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ch.tdussuet.fuellog.R;
import ch.tdussuet.fuellog.db.FuelDatabase;
import ch.tdussuet.fuellog.db.dao.GasRecordDao;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.db.entity.Vehicle;
import ch.tdussuet.fuellog.util.Utilities;
import lombok.Setter;
import timber.log.Timber;

/**
 * DESCRIPTION:
 * Implements an abstract log for the storage of gasoline records. Provides
 * CRUD methods to create, read, update, and delete gasoline records from
 * the log. The underlying storage mechanism is an SQLite database.
 */
public class GasRecordExporter implements Serializable {
    private static final String TAG = GasRecordExporter.class.getName();

    private static final DateTimeFormatter csvDateFormatter =
            DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US);
    private static final DateTimeFormatter csvDateTimeFormatter =
            DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm", Locale.US);

    @Setter
    private transient Context context;

    private GasRecordExporter() {}

    public GasRecordExporter(Context context) {
        this.context = context;
    }

    /**
     * DESCRIPTION:
     * Reads gasoline record data from a specified ASCII CSV formatted file
     * into the log for a specific vehicle.
     *
     * @param vehicle - the Vehicle to import records for.
     * @param file    - the ASCII CSV data file to import.
     * @return boolean flag indicating success/failure (true=success)
     */
    public boolean importData(Vehicle vehicle, InputStream file) {

        final String tag = TAG + ".importData()";

        boolean success = false;

        FuelDatabase database = Room.databaseBuilder(context.getApplicationContext(), FuelDatabase.class, FuelDatabase.DB_NAME).build();
        GasRecordDao gasRecordDao = database.gasRecordDao();
        List<GasRecord> gasRecords = new ArrayList<>();

        int num = 0;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(file))) {
            String line;
            do {
                line = reader.readLine();
                num++;
                if (line != null && !line.startsWith("#")) {
                    GasRecord gasRecord = getGasRecordfromCSVLine(line);
                    gasRecord.setVehicleID(vehicle.getId());
                    gasRecords.add(gasRecord);
                }
            } while (line != null);

            gasRecordDao.insertAll(gasRecords).doOnError(e -> {
                Timber.tag(tag).e(e, "import database insert failed");
                String errorMessage = context.getString(R.string.toast_import_failed);
                Utilities.toast(context, errorMessage);
            }).blockingAwait();

            success = true;

        } catch (Exception e) {
            Timber.tag(tag).e(e, "import failed");
            String format = context.getString(R.string.toast_stopped_at_csv_line);
            Utilities.toast(context, String.format(format, num));
        }

        return success;
    }

    /**
     * DESCRIPTION:
     * Copies all existing log data for a specific vehicle to an ASCII CSV file.
     *
     * @param vehicle - the Vehicle to export data for.
     * @param file    - the ASCII CSV file to create.
     * @return boolean flag indicating success/failure (true=success)
     */
    public boolean exportData(Vehicle vehicle, Uri file) {

        final String tag = TAG + ".exportData()";

        boolean status = false;

        FuelDatabase database = Room.databaseBuilder(context.getApplicationContext(), FuelDatabase.class, FuelDatabase.DB_NAME).build();
        GasRecordDao gasRecordDao = database.gasRecordDao();
        List<GasRecord> gasRecords = gasRecordDao.findAllByVehicle(vehicle.getId()).blockingGet();

        try (BufferedWriter bufferedwriter = new BufferedWriter(new OutputStreamWriter(context.getContentResolver().openOutputStream(file)))) {
            for (GasRecord gasRecord : gasRecords) {
                bufferedwriter.write(getCSVLine(gasRecord));
                bufferedwriter.newLine();
            }
            bufferedwriter.flush();
            status = true;
        } catch (Exception t) {
            Timber.tag(tag).e(t, "export failed");
        }

        return status;
    }

    /**
     * DESCRIPTION:
     * Constructs an instance of GasRecord reflecting values specified
     * by an ASCII comma-separated-values (CSV) String.<p>
     * <p>
     * NOTES:<p>
     * <ol>
     * <li>This method needs to be backwards compatible with CSV
     * data created with previous versions of the application database.
     *     <ul>
     *     <li>formatting prior to database version 5 was:<br>
     *     <pre>date,odometer,gallons,fulltank,hidden,[calculation]</pre>
     *     <li>formatting for database version 5 is:<br>
     *     <pre>date,odometer,gallons,fulltank,hidden,cost,notes,[calculation]</pre>
     *     </ul>
     * <li>Calculation values are for user information only in the CSV. They
     * are ignored in this constructor and re-calculated later if necessary.
     * </ol>
     *
     * @param csvLine - the ASCI CSV String that specified the record's values.
     * @throws ParseException        if parse of CSV fails.
     * @throws NumberFormatException if CSV contains invalid numeric values.
     */
    public GasRecord getGasRecordfromCSVLine(String csvLine) throws ParseException, NumberFormatException {
        String[] values = csvLine.split(",");
        GasRecord gasRecord = new GasRecord();

        switch (values.length) {
            case 8:  // db_version=5 with calculation
            case 7:  // db_version=5
                gasRecord.setDate(getCSVDateTime(values[0]));
                gasRecord.setOdometer(Integer.parseInt(values[1]));
                gasRecord.setAmount(Float.parseFloat(values[2]));
                gasRecord.setFullTank(Boolean.parseBoolean(values[3]));
                gasRecord.setHidden(Boolean.parseBoolean(values[4]));
                gasRecord.setCost(Double.parseDouble(values[5]));
                gasRecord.setNotes(values[6]);
                break;

            case 6:  // db_version<5 with calculation
            case 5:  // db_version<5
                gasRecord.setDate(getCSVDateTime(values[0]));
                gasRecord.setOdometer(Integer.parseInt(values[1]));
                gasRecord.setAmount(Float.parseFloat(values[2]));
                gasRecord.setFullTank(Boolean.parseBoolean(values[3]));
                gasRecord.setHidden(Boolean.parseBoolean(values[4]));
                gasRecord.setCost(0d);
                gasRecord.setNotes("");
                break;

            default:
                throw new ParseException("Invalid CSV length", values.length);
        }

        return gasRecord;
    }

    private LocalDateTime getCSVDateTime(String date) throws DateTimeParseException {
        try {
            // assume date & time specified in string
            return LocalDateTime.parse(date, csvDateTimeFormatter);
        } catch (DateTimeParseException e) {
            //...but also allow just date
            return LocalDateTime.parse(date, csvDateFormatter);
        }
    }

    /**
     * DESCRIPTION:
     * Returns an ASCII CSV String representation of the record.
     *
     * @return String reflecting record attribute values.
     */
    private String getCSVLine(GasRecord gasRecord) {
        var date = gasRecord.getDate().format(csvDateTimeFormatter);
        var fuelAmount = prepareNumberForCSV(GasRecordValidator.getAmountString(context, gasRecord));
        var cost = prepareNumberForCSV(GasRecordValidator.getCostString(context, gasRecord));
        var mileage = prepareNumberForCSV(GasRecordValidator.getAmountString(context, gasRecord));
        var notes = prepareStringForCSV(GasRecordValidator.validateNotes(gasRecord.getNotes()));

        return date + "," +
                gasRecord.getOdometer() + "," +
                fuelAmount + "," +
                gasRecord.isFullTank() + "," +
                gasRecord.isHidden() + "," +
                cost + "," +
                notes + "," +
                mileage;
    }

    private String prepareNumberForCSV(String number) {
        return number.replace(',', '.');
    }

    private String prepareStringForCSV(String string) {
        return string.replace(',', ' ').replace('\n', ' ');
    }
}
