package ch.tdussuet.fuellog.item;

import androidx.annotation.NonNull;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import ch.tdussuet.fuellog.db.entity.GasRecord;

public class GasRecordCalculator {

    private GasRecordCalculator() {}

    /**
     * DESCRIPTION:
     * Calculates price based on the current values for cost and gallons.
     *
     * @throws NumberFormatException if the calculated value is not a valid price value.
     */
    public static double calculatePrice(GasRecord gasRecord) {
        double value = 0d;
        if (gasRecord.getAmount() != 0) {
            value = gasRecord.getCost() / gasRecord.getAmount();
        }
        return GasRecordValidator.validatePrice(value);
    }

    /**
     * DESCRIPTION:
     * Calculates gallons based on the current values for cost and price.
     *
     * @throws NumberFormatException if the calculated value is not a valid gallons value.
     */
    public static float calculateAmount(GasRecord gasRecord) {
        float value = 0f;
        if (gasRecord.getPrice() != 0) {
            value = (float) (gasRecord.getCost() / gasRecord.getPrice());
        }
        return GasRecordValidator.validateAmount(value);
    }

    /**
     * DESCRIPTION:
     * Calculates cost based on the current values for gallons and price.
     *
     * @throws NumberFormatException if the calculated value is not a valid cost value.
     */
    public static double calculateCost(GasRecord gasRecord) {
        double value = gasRecord.getPrice() * gasRecord.getAmount();
        return GasRecordValidator.validateCost(value);
    }

    @NonNull
    public static List<TripRecord> getMonthlySummary(List<GasRecord> gasRecords) {
        if (gasRecords == null || gasRecords.size() < 2) {
            return Collections.emptyList();
        }
        var tripRecords = new HashMap<LocalDateTime, TripRecord>();
        var previousGasRecord = gasRecords.get(0);
        for (GasRecord gasRecord : gasRecords) {
            var month = getMonth(gasRecord);
            tripRecords.putIfAbsent(month, new TripRecord(previousGasRecord.getDate(), previousGasRecord));
            if (gasRecord != previousGasRecord) {
                Objects.requireNonNull(tripRecords.get(month)).append(gasRecord);
            }
            previousGasRecord = gasRecord;
        }
        return new ArrayList<>(tripRecords.values());
    }

    private static LocalDateTime getMonth(GasRecord gasRecord) {
        return gasRecord.getDate().toLocalDate().withDayOfMonth(1).atStartOfDay();
    }
}
