/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.item;

import android.content.Context;

import java.text.DateFormat;

import ch.tdussuet.fuellog.App;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.util.format.CurrencyManager;

public class GasRecordValidator {

    // define maximum values (for display reasons)
    public static final int MAX_ODOMETER = 9999999;
    public static final float MAX_GALLONS = 9999.999f;
    public static final double MAX_COST = 999999.999d;
    public static final double MAX_PRICE = 999999.999d;
    public static final String VALUE_OUT_OF_RANGE = "Value out of range.";

    private GasRecordValidator() {}

    /**
     * DESCRIPTION:
     * Getter method for the date attribute as a String value.
     *
     * @return String - the date value (MM/dd/yyyy).
     */
    public static String getDateString(Context context, GasRecord gasRecord) {
        return getDateFormatter(context).format(gasRecord.getDate());
    }

    private static DateFormat getDateFormatter(Context context) {
        return android.text.format.DateFormat.getDateFormat(context);
    }

    private static DateFormat getTimeFormatter(Context context) {
        return android.text.format.DateFormat.getTimeFormat(context);
    }

    /**
     * DESCRIPTION:
     * Getter method for the date attribute as a date/time String value.
     *
     * @return String - the date/time value.
     */
    public static String getDateTimeString(Context context, GasRecord gasRecord) {
        return getDateFormatter(context).format(gasRecord.getDate()) +
                " " +
                getTimeFormatter(context).format(gasRecord.getDate());
    }

    /**
     * DESCRIPTION:
     * Setter method for the odometer attribute as a String value.
     *
     * @param odometer - the odometer String value.
     * @throws NumberFormatException if the String is not a valid odometer value.
     */
    public static int validateOdometer(String odometer) throws NumberFormatException {
        int value = Integer.parseInt(odometer);
        return validateOdometer(value);
    }

    public static int validateOdometer(int value) {
        return validateNumber(value, MAX_ODOMETER);
    }

    /**
     * DESCRIPTION:
     * Getter method for the gallons attribute as a String value.
     *
     * @return String - the gallons value.
     */
    public static String getAmountString(Context context, GasRecord gasRecord) {
        return String.format(App.getLocale(context), "%.3f", gasRecord.getAmount());
    }

    public static float validateAmount(String amount) throws NumberFormatException {
        float value;
        value = Float.parseFloat(amount.replace(',', '.'));
        return validateAmount(value);
    }

    public static float validateAmount(float value) {
        return validateNumber(value, MAX_GALLONS);
    }

    /**
     * DESCRIPTION:
     * Getter method for the cost attribute as a String value.
     *
     * @return String - the cost value.
     */
    public static String getCostString(Context context, GasRecord gasRecord) {
        return CurrencyManager.getInstance(context).getNumericFormatter().format(gasRecord.getCost());
    }

    public static double validateCost(String cost) throws NumberFormatException {
        double value = Double.parseDouble(cost.replace(',', '.'));
        return validateCost(value);
    }

    public static double validateCost(double value) {
        return validateNumber(value, MAX_COST);
    }

    /**
     * DESCRIPTION:
     * Getter method for the price per gallon attribute as a String value.
     *
     * @return String - the price value.
     */
    public static String getPriceString(Context context, GasRecord gasRecord) {
        return CurrencyManager.getInstance(context).getNumericFractionalFormatter().format(gasRecord.getPrice());
    }

    public static double validatePrice(String price) {
        double value = Double.parseDouble(price.replace(',', '.'));
        return validatePrice(value);
    }

    public static double validatePrice(double price) {
        return validateNumber(price, MAX_PRICE);
    }

    private static <G extends Number> G validateNumber(G value, G maxPrice) {
        if (value.doubleValue() < 0 || value.doubleValue() > maxPrice.doubleValue()) {
            throw new NumberFormatException(VALUE_OUT_OF_RANGE);
        }
        return value;
    }

    public static String validateNotes(String notes) {
        if (notes == null) notes = "";
        return notes;
    }
}