/*
 * *****************************************************************************
 * Copyright 2021 Thierry Dussuet
 * Copyright 2013 William D. Kraemer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ****************************************************************************
 */

package ch.tdussuet.fuellog.item;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import ch.tdussuet.fuellog.db.entity.GasRecord;
import lombok.Getter;
import lombok.Setter;

/**
 * DESCRIPTION:
 * Represents data for a trip that spans start and end points defined
 * by two gas records.
 */
@Getter
@Setter
public class TripRecord {

    /// the date the trip started
    private LocalDateTime startDate;

    /// the date the trip ended
    private LocalDateTime endDate;

    private int startOdometer;
    private int endOdometer;

    /// the amount of gasoline purchased 
    private double amount;

    /// the cost of the gasoline purchased
    private double cost;

    // the set of gas records that the trip represents
    private final Set<GasRecord> gasRecords = new HashSet<>();

    /**
     * DESCRIPTION:
     * Constructs an empty instance of TripRecord for a specified date
     *
     * @param date - the start/end date for the trip.
     */
    public TripRecord(LocalDateTime date) {
        startDate = date;
        endDate = date;
        startOdometer = 0;
        endOdometer = 0;
        amount = 0f;
        cost = 0d;
    }

    public TripRecord(LocalDateTime date, GasRecord gasRecord) {
        startDate = date;
        endDate = date;
        startOdometer = gasRecord.getOdometer();
        endOdometer = gasRecord.getOdometer();
        amount = 0f;
        cost = 0d;
    }

    public TripRecord(GasRecord... gasRecords) {
        var firstGasRecord = gasRecords[0];
        var lastGasRecord = gasRecords[gasRecords.length - 1];
        startDate = firstGasRecord.getDate();
        endDate = lastGasRecord.getDate();
        endOdometer = lastGasRecord.getOdometer();
        startOdometer = firstGasRecord.getOdometer();
        for (int i = 1; i < gasRecords.length; i++) {
            amount += gasRecords[i].getAmount();
            cost += gasRecords[i].getCost();
            this.gasRecords.add(gasRecords[i]);
        }
    }

    /**
     * DESCRIPTION:
     * Append the data for another trip to this trip record, such
     * that this trip record now reflects the totals for both trips.
     *
     * @param that - the TripRecord to append.
     */
    public void append(TripRecord that) {

        // take the earliest start date
        if (that.startDate.isBefore(this.startDate)) {
            this.startDate = that.startDate;
        }

        // take the latest end date
        if (that.endDate.isAfter(this.endDate)) {
            this.endDate = that.endDate;
        }

        if (that.endOdometer > this.endOdometer) {
            this.endOdometer = that.endOdometer;
        }

        if (that.startOdometer < this.startOdometer) {
            this.startOdometer = that.startOdometer;
        }

        // calculate totals for both trips
        this.amount += that.amount;
        this.cost += that.cost;
        this.gasRecords.addAll(that.gasRecords);
    }

    public void append(GasRecord gasRecord) {
        if (gasRecord.getDate().isBefore(this.startDate)) {
            this.startDate = gasRecord.getDate();
        }
        if (gasRecord.getDate().isAfter(this.endDate)) {
            this.endDate = gasRecord.getDate();
        }
        if (gasRecord.getOdometer() < this.startOdometer) {
            this.startOdometer = gasRecord.getOdometer();
        }
        if (gasRecord.getOdometer() > this.endOdometer) {
            this.endOdometer = gasRecord.getOdometer();
        }
        this.amount += gasRecord.getAmount();
        this.cost += gasRecord.getCost();
        this.gasRecords.add(gasRecord);
    }

    /**
     * DESCRIPTION:
     * Getter method for average price per gallon paid for fuel
     * over the trip period.
     *
     * @return the price of fuel per gallon.
     */
    public Double getPrice() {
        double price = 0.0d;
        if (amount > 0) {
            price = cost / amount;
        }
        return price;
    }

    public double getDistance() {
        return (double) endOdometer - startOdometer;
    }
}
