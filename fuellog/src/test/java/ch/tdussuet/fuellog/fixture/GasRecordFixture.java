package ch.tdussuet.fuellog.fixture;

import java.time.LocalDateTime;

import ch.tdussuet.fuellog.db.entity.GasRecord;

public class GasRecordFixture {
    private GasRecordFixture() {}

    public static GasRecord getGasRecord() {
        return GasRecord.builder()
                .vehicleID(1)
                .amount(20f)
                .cost(20f)
                .fullTank(true)
                .odometer(1234)
                .date(LocalDateTime.now())
                .build();
    }
}
