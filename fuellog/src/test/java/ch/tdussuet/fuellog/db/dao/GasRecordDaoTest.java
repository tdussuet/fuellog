package ch.tdussuet.fuellog.db.dao;

import static ch.tdussuet.fuellog.fixture.GasRecordFixture.getGasRecord;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import ch.tdussuet.fuellog.db.FuelDatabase;
import ch.tdussuet.fuellog.db.entity.GasRecord;

@RunWith(AndroidJUnit4.class)
class GasRecordDaoTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule =
            new InstantTaskExecutorRule();

    private FuelDatabase fuelDatabase;
    private GasRecordDao gasRecordDao;

    @BeforeEach
    void setUp() {
        fuelDatabase = Room.inMemoryDatabaseBuilder(
                ApplicationProvider.getApplicationContext(),
                FuelDatabase.class)
                .allowMainThreadQueries()
                .build();
        gasRecordDao = fuelDatabase.gasRecordDao();
    }

    @AfterEach
    void tearDown() {
        fuelDatabase.close();
    }

    @Test
    void getAll() {
        gasRecordDao.getAll()
                .test()
                .assertEmpty();
    }

    @Test
    void findAllByVehicle() {
        var gasRecord = getGasRecord();
        gasRecordDao.insert(gasRecord)
                .test()
                .assertNoErrors();
        gasRecordDao.findAllByVehicle(gasRecord.getVehicleID())
                .test()
                .assertValue(gasRecordList -> gasRecordList.size() == 1);
    }

    @Test
    void getCurrentOdometer() {
        var gasRecord = getGasRecord();
        gasRecordDao.insert(gasRecord)
                .test()
                .assertNoErrors();
        gasRecordDao.getCurrentOdometer(gasRecord.getVehicleID())
                .test()
                .assertValue(gasRecord.getOdometer());
    }

    @Test
    void insert() {
        GasRecord gasRecord = getGasRecord();
        gasRecordDao.insert(gasRecord)
                .test()
                .assertNoErrors();
    }

    @Test
    void insertAll() {
        var gasRecords = new ArrayList<GasRecord>();
        gasRecords.add(getGasRecord());

        gasRecordDao.insertAll(gasRecords)
                .test()
                .assertNoErrors();
    }

    @Test
    void update() {
        GasRecord gasRecord = getGasRecord();
        gasRecordDao.insert(gasRecord)
                .test()
                .assertNoErrors();
        var cost = gasRecord.getCost();
        var newCost = cost * 2;
        gasRecord.setCost(newCost);
        gasRecordDao.update(gasRecord)
                .test()
                .assertNoErrors();
        gasRecordDao.getAll()
                .test()
                .assertValue(gasRecordList -> gasRecordList.get(0).getCost() == newCost);
    }

    @Test
    void delete() {
        GasRecord gasRecord = getGasRecord();
        gasRecordDao.insert(gasRecord)
                .test()
                .assertNoErrors();
        gasRecordDao.delete(gasRecord)
                .test()
                .assertNoErrors();
        gasRecordDao.getAll()
                .test()
                .assertEmpty();
    }
}