package ch.tdussuet.fuellog.plot;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import ch.tdussuet.fuellog.activity.PlotActivity;
import ch.tdussuet.fuellog.db.entity.GasRecord;
import ch.tdussuet.fuellog.preferences.Units;
import ch.tdussuet.fuellog.util.MileageCalculation;

class MileagePlotTest {

    MileagePlot mileagePlot;

    MockedStatic<PreferenceManager> preferenceManager;
    MockedStatic<DateFormat> dateFormat;

    @BeforeEach
    void setUp() {
        mileagePlot = new MileagePlot();
        mileagePlot.activity = Mockito.mock(PlotActivity.class);
        Resources resources = Mockito.mock(Resources.class);
        when(resources.getStringArray(anyInt())).thenReturn(new String[]{"0"});
        when(mileagePlot.activity.getResources()).thenReturn(resources);
        final SharedPreferences sharedPrefs = Mockito.mock(SharedPreferences.class);
        when(sharedPrefs.getString(anyString(), anyString())).thenReturn("0");
        preferenceManager = Mockito.mockStatic(PreferenceManager.class);
        preferenceManager.when(() -> PreferenceManager.getDefaultSharedPreferences(any())).thenReturn(sharedPrefs);
        dateFormat = Mockito.mockStatic(DateFormat.class);
        dateFormat.when(() -> DateFormat.getDateFormat(any())).thenReturn(Mockito.mock(java.text.DateFormat.class));
    }

    @AfterEach
    void tearDown() {
        preferenceManager.close();
        dateFormat.close();
    }

    @Test
    void shouldCalculateEmptyAverage() {
        mileagePlot.getPlotSeries();
        Assertions.assertEquals(0, mileagePlot.average);
    }

    @Test
    void shouldCalculateSinglePointAverage() {
        List<GasRecord> gasRecords = new ArrayList<>();
        gasRecords.add(getFirstGasRecord());
        Mockito.when(((PlotActivity)mileagePlot.activity).getRecords()).thenReturn(gasRecords);
        mileagePlot.getPlotSeries();
        Assertions.assertEquals(0, mileagePlot.average);
    }

    @Test
    void shouldCalculateTwoPointsAverage() {
        List<GasRecord> gasRecords = getGasRecords();
        Mockito.when(((PlotActivity)mileagePlot.activity).getRecords()).thenReturn(gasRecords);
        mileagePlot.getPlotSeries();
        Assertions.assertEquals(50.0, mileagePlot.average);
    }

    private List<GasRecord> getGasRecords() {
        List<GasRecord> gasRecords = new ArrayList<>();
        GasRecord gasRecord = getFirstGasRecord();
        gasRecords.add(gasRecord);

        GasRecord secondGasRecord = getGasRecord(1000, gasRecord);
        gasRecords.add(secondGasRecord);
        return gasRecords;
    }

    private GasRecord getFirstGasRecord() {
        Units units = new Units("", mileagePlot.activity);
        GasRecord gasRecord = getGasRecord(500);
        MileageCalculation mileageCalculation = new MileageCalculation(gasRecord, units, mileagePlot.activity);
        gasRecord.setMileage(mileageCalculation.getMileage());
        return gasRecord;
    }

    private GasRecord getGasRecord(int odometer, GasRecord firstGasRecord) {
        GasRecord gasRecord = getGasRecord(odometer);
        Units units = new Units("", mileagePlot.activity);
        MileageCalculation mileageCalculation = new MileageCalculation(firstGasRecord, units, mileagePlot.activity);
        mileageCalculation.add(gasRecord);
        gasRecord.setMileage(mileageCalculation.getMileage());
        return gasRecord;
    }

    private GasRecord getGasRecord(int odometer) {
        GasRecord gasRecord = new GasRecord();
        gasRecord.setOdometer(odometer);
        gasRecord.setCost(50.0);
        gasRecord.setCost(100.0);
        gasRecord.setDate(LocalDateTime.now());
        gasRecord.setAmount(10.0F);
        gasRecord.setFullTank(true);
        return gasRecord;
    }
}