package ch.tdussuet.fuellog.activity;

import org.junit.jupiter.api.Test;

import java.util.Calendar;

class DateTimeActivityTest {

    @Test
    void getYear() {
        Calendar date = Calendar.getInstance();
        int year = date.get(Calendar.YEAR);
        System.out.println("year = " + year);
    }
}