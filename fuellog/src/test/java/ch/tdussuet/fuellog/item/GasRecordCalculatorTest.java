package ch.tdussuet.fuellog.item;

import static ch.tdussuet.fuellog.fixture.GasRecordFixture.getGasRecord;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import ch.tdussuet.fuellog.db.entity.GasRecord;

class GasRecordCalculatorTest {

    @Test
    void calculatePrice() {
    }

    @Test
    void calculateAmount() {
    }

    @Test
    void calculateCost() {
    }

    @Test
    void getMonthlySummaryWithEmptyList() {
        List<GasRecord> gasRecords = new ArrayList<>();
        var tripRecords = GasRecordCalculator.getMonthlySummary(gasRecords);
        Assertions.assertNotNull(tripRecords);
    }

    @Test
    void getMonthlySummaryWithOneEntry() {
        List<GasRecord> gasRecords = new ArrayList<>();
        GasRecord gasRecord = getGasRecord();
        gasRecords.add(gasRecord);
        var tripRecords = GasRecordCalculator.getMonthlySummary(gasRecords);
        Assertions.assertNotNull(tripRecords);
        Assertions.assertTrue(tripRecords.isEmpty());
    }

    @Test
    void getMonthlySummaryWithTwoEntries() {
        List<GasRecord> gasRecords = new ArrayList<>();
        GasRecord gasRecord = getGasRecord();
        GasRecord secondGasRecord = getGasRecord();
        gasRecords.add(gasRecord);
        gasRecords.add(secondGasRecord);

        var tripRecords = GasRecordCalculator.getMonthlySummary(gasRecords);

        Assertions.assertNotNull(tripRecords);
        Assertions.assertEquals(1, tripRecords.size());
        var tripRecord = tripRecords.get(0);
        Assertions.assertEquals(secondGasRecord.getCost(), tripRecord.getCost());
    }

    @Test
    void getMonthlySummaryWithTwoTrips() {
        List<GasRecord> gasRecords = new ArrayList<>();
        GasRecord gasRecord = getGasRecord();
        gasRecord.setDate(LocalDateTime.now().minusMonths(3));
        GasRecord secondGasRecord = getGasRecord();
        secondGasRecord.setDate(LocalDateTime.now().minusMonths(2));
        GasRecord thirdGasRecord = getGasRecord();
        thirdGasRecord.setDate(LocalDateTime.now().minusMonths(1));
        gasRecords.add(gasRecord);
        gasRecords.add(secondGasRecord);
        gasRecords.add(thirdGasRecord);
        var tripRecords = GasRecordCalculator.getMonthlySummary(gasRecords);
        Assertions.assertNotNull(tripRecords);
        Assertions.assertEquals(3, tripRecords.size());
        var tripRecord = tripRecords.get(2);
        Assertions.assertEquals(secondGasRecord.getCost(), tripRecord.getCost());
    }
}